<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diradmin extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('login/login_form');
		}
		$this->load->model('Adminmodel');
		$this->load->model('Dirmodel');
		$this->data['notifications'] = $this->Adminmodel->getnotify();
    }
	 
	public function index()
	{
		// admin home page has search for users, departments. 
		$this->data['title'] = 'Welcome';
		$this->data['message'] = '';
		$this->data['content'] = 'admin/welcome';
		  
	 	$this->load->view('admin/base', $this->data);	

	}
	
	public function  notification(){
		// admin home page has search for users, departments. 
				
		$type = $this->uri->segment(3);
		$title = rtrim(urldecode(base64_decode($this->uri->segment(4))), '7');
		$message = rtrim(urldecode(base64_decode($this->uri->segment(5))), '7');

		
		$this->data['title'] = $title;
		$this->data['message'] = $message;
		$this->data['type'] = $type;
		$this->data['content'] = 'admin/notification';
		  
		// get all the information about the transaction and send to the notification window
		
		$trans_id = $this->uri->segment(6);
		if(is_numeric($trans_id)){
		 $trans = $this->gettrans($trans_id);
			switch($trans[0]['trans_type']){
				case 'new-emp':
				$this->data['title'] = 'New Employee Created';
				break;
				case 'new-dept':
				$this->data['title'] = 'New Department Created';
				break;
				case 'up-emp':
				$this->data['title'] = 'Employee Updated';
				break;
				case 'up-dept':
				$this->data['title'] = 'Department Updated';
				break;
				default:
				$this->data['title'] = 'Success';
			}
		 $this->data['message'] = $trans[0]['desc'] ;
		}
		
	 	$this->load->view('admin/base', $this->data);	
	}

	function listdept(){
		// List all departments 
		$depts = $this->Dirmodel->dept('', '', '', $this->uri->segment(3));
		
		// get all contact info. 
		for($i=0; $i < count($depts); $i++){
			
			$depts[$i]['slo'] = $this->Dirmodel->deptloc($depts[$i]['id'], 1, $this->uri->segment(3));
			$depts[$i]['nc'] = $this->Dirmodel->deptloc($depts[$i]['id'], 2, $this->uri->segment(3));
			$depts[$i]['sc'] = $this->Dirmodel->deptloc($depts[$i]['id'], 3, $this->uri->segment(3));
			
		}
		
		
		$this->data['title'] = 'Departments';
		$this->data['depts'] = $depts;
		$this->data['content'] = 'admin/listdept';
	
		
		 $this->load->view('admin/base', $this->data);
		
	}

	function listemp(){
		// Get all employees 
		$emps = $this->Dirmodel->emp('', '', $this->uri->segment(3));

		$this->data['title'] = 'Employees';
		$this->data['emp'] = $emps;
		$this->data['content'] = 'admin/listemp';
		
		if(is_numeric($this->uri->segment(3))){
			$this->data['dept'] = $this->Dirmodel->dept($this->uri->segment(3));
		}
		
		 $this->load->view('admin/base', $this->data);
	}
	
	
	function adddept(){
		// Add Department Window. 
		
		$this->data['title'] = 'Add New Department';
		$this->data['content'] = 'admin/newdept';  

		$this->load->view('admin/base', $this->data);

	
	}
	
	
	function updept($id) {
			
		// update employee contact info, etc.
		if(!is_numeric($id) || $id < 1 ){
			redirect('diradmin/');
		}
			
		// get employee info	
		$dept = $this->Dirmodel->dept($id, '', false);
		
		if(empty($dept)){
			redirect('diradmin/');
		}
		
				// get location data;
		$slo = $this->Dirmodel->deptloc($id, 1);
		$nc = $this->Dirmodel->deptloc($id, 2);
		$sc = $this->Dirmodel->deptloc($id, 3);
		
		// title 
		$title = 'Update Entry: ' . $dept[0]['dname'];
		
			
       $this->data['title'] = $title;
		$this->data['message'] = '';
		$this->data['content'] = 'admin/editdepartment';
		$this->data['dept_id'] = $id;
		$this->data['dept'] = $dept;
		$this->data['slo'] = $slo;
		$this->data['nc'] = $nc;
		$this->data['sc'] = $sc;
		 
		 $this->load->view('admin/base', $this->data);
		
		
		
	
	}
	
	function remdept(){
		$this->db->where('dactive', 0);
		$q = $this->db->get('dept');
		$result = $q->result_array();
		

		$this->data['title'] = 'Removed Employees';
		$this->data['message'] = '';
		$this->data['content'] = 'admin/remdept';
		$this->data['depts'] = $result;

		$this->load->view('admin/base', $this->data);
	}
	
	
	
	/*  ======================
		Employee Admin Functions
		=======================
	*/
	
	function addemp($emp = array()) {
	  // add employee
	  	
	  	// get all the departments 
	  	$this->db->where('dactive', 1);
	  	$query = $this->db->get('dept');		
		$depts = $query->result_array();

		  
		$this->data['title'] = 'Add New Employee';
		$this->data['message'] = '';
		$this->data['content'] = 'admin/newemployee';
		$this->data['depts'] = $depts;

		$this->load->view('admin/base', $this->data);
	  

	}
	
	function upemp($id) {
			
		// update employee contact info, etc.
		if(!is_numeric($id) || $id < 1){
			redirect('diradmin/');
		}
			
		// get employee info	
		$emp = $this->Dirmodel->emp($id, "", "",  "", false);
		
		if(empty($emp)){
			redirect('diradmin/');
		}
		
		// get departments (and ones employee is associated with)
		$depts = $this->Dirmodel->deptemp($id);
		
		// get location data;
		$slo = $this->Dirmodel->emploc($id, 1);
		$nc = $this->Dirmodel->emploc($id, 2);
		$sc = $this->Dirmodel->emploc($id, 3);
		
		// title 
		$title = 'Update Entry: ' . $emp[0]['fname'] . ' ' . $emp[0]['lname'];
					
		 
		$this->data['title'] = $title;
		$this->data['message'] = '';
		$this->data['content'] = 'admin/editemployee';
		$this->data['emp_id'] = $id;
		$this->data['emp'] = $emp;
		$this->data['depts'] = $depts;
		$this->data['slo'] = $slo;
		$this->data['nc'] = $nc;
		$this->data['sc'] = $sc;
         
		 
		$this->load->view('admin/base', $this->data);
	   
		
	}
	
	function rememp(){
		$this->db->where('active', 0);
		$q = $this->db->get('emp');
		$result = $q->result_array();
		

		  
		$this->data['title'] = 'Removed Employees';
		$this->data['message'] = '';
		$this->data['content'] = 'admin/rememployee';
		$this->data['emp'] = $result;

		$this->load->view('admin/base', $this->data);
		
		
	}
	
	function notifications(){
		
		$this->data['title'] = 'eDirectory Logs';
		$this->data['message'] = '';
		$this->data['dump'] = $this->Adminmodel->getnotify(true);
		$this->data['content'] = 'admin/notification_dump';
		
		$this->load->view('admin/base', $this->data);

		
	}
	
	/* Internal Functions */
	
	function check_req($arr){
		// check required
		$errors = array();
		foreach ($arr as $key => $value) {
			 if(strpos($key,'_r') !== false && $value == ""){
			 	$newkey = str_replace('_r', '', $key);
				$error[$newkey] = 1;
			 }
		}
		
		return $errors;
	
	}
	
	function clean_req($arr){
		// removes the required _r from the key.. 
		$new = array();
		foreach ($arr as $key => $value) {
			 if(strpos($key,'_r') !== false){
				$newkey = str_replace('_r', '', $key);
				$new[$newkey] = $value;
			 } else {
			 	$new[$key] = $value;
			 }
		}
		return $new;
		
	}
	
	
	private function gettrans($trans_id, $detail = false){
		// get transaction description
		$this->db->where('id', $trans_id);
		$tran_q = $this->db->get('trans');
		$tran_r = $tran_q->result_array();
		
		// TODO: get more detailed information on logs. 
		if($detail){
				
			$this->db->where('trans_id', $trans_id);
			$query = $this->db->get('logs');
			$logr = $query->result_array();
			$countlogs = count($logr);
			for($i=0; $i<$countlogs; $i++){
			
			}
		}
		
		return $tran_r;
		
	}
	
}