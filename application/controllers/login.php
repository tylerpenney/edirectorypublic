<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {


	public function index()
	{
		redirect('login/login_form');
	}
	
	public function login_form(){
		$this->load->view('admin/adminlogin');
	}
	
	    /**
     * login user
     */
    public function adminlogin() {
        //default response
        $this->response = array('status' => 0,
            'message' => 'Unexpected error.',
            'errors' => array());

        $this->load->library('form_validation');

        //validate form
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');


        if ($this->form_validation->run() == FALSE) {
             redirect('login/login_form/error');
        } else {
			$userp = $this->input->post('password');
			
			$this->db->where('user_name', $this->input->post('email'));
			$this->db->limit(1);
			$query = $this->db->get('tmp_admin');
			$dbuser = $query->result_array();
			
           if(!empty($dbuser) && password_verify($userp, $dbuser[0]['pss'] )) { // user exists!
				
				unset($dbuser[0]['pss']);	
		   		$newdata = array(
		   				   'id' => $dbuser[0]['id'],
		   				   'name' => $dbuser[0]['real_name'],
                		   'email' => $dbuser[0]['user_name'],
                   	   	   'logged_in' => TRUE
               	);	
				
				$this->session->set_userdata($newdata);
				redirect('diradmin');		

            } else {
           
           		redirect('login/login_form/error');
			}
        }

    }

    /**
     * user logout
     */
    public function logout() {
       		$this->session->sess_destroy();
        redirect(site_url('login/login_form/logout'), 'refresh');
    }
}
