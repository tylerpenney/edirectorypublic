<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dirajax extends CI_Controller {

	var $user_id;

	public function __construct()
    {
        parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('login/login_form');
		}
		$this->load->model('Adminmodel');
		$this->load->model('Dirmodel');
		$this->response = array('status' => 0,
            'message' => 'Unexpected error.',
            'errors' => array());
		$this->user_id = 1;
		
    }
	 
	public function index()
	{
		// admin home page has search for users, departments. 
		$data = array(
               'title' => 'Welcome',
               'message' => '',
			   'content' => 'admin/welcome'
			   
          );
		  
	 	$this->load->view('admin/base', $data);	
		
	}
	
	
	function adddept(){
		// Add Department
		$add = array( 
					  'dname_r' => trim($this->input->post('dname')),
					  'ddesc' => $this->input->post('ddesc'),
					  'durl_r' => $this->input->post('durl'),
					  'daca' => $this->input->post('daca'),
					  'dnon' => $this->input->post('dnon')
				);
				
		$slo = array(
					'lbuild' => $this->input->post('slo_lbuild'),
					'lphone' => $this->input->post('slo_lphone'),
					'lext' => $this->input->post('slo_lext'),
					'lfax' => $this->input->post('slo_lfax')
				);
				
		$nc = array(
					'lbuild' => $this->input->post('nc_lbuild'),
					'lphone' => $this->input->post('nc_lphone'),
					'lext' => $this->input->post('nc_lext'),
					'lfax' => $this->input->post('nc_lfax')
				);
				
		$sc = array(
					'lbuild' => $this->input->post('sc_lbuild'),
					'lphone' => $this->input->post('sc_lphone'),
					'lext' => $this->input->post('sc_lext'),
					'lfax' => $this->input->post('sc_lfax')
				);
				

				
		$missing = $this->check_req($add);		
		
		$this->db->where('dname', $add['dname_r']);
		$query = $this->db->get('dept');
		$rowcount = $query->num_rows();
		
		if($rowcount > 0){
					$type = 'error';
					$title = urlencode(base64_encode("Error Duplication"));
					$message = urlencode(base64_encode("The department already exists"));
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
		}	
		
		if(!empty($missing)){
			
		
			$type = 'error';
			$title = urlencode(base64_encode("An Error Occured"));
			$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
			redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
			
		} else {
			
			$add = $this->clean_req($add);
			
			$try = $this->Adminmodel->adddept($add, $slo, $sc, $nc);	
			
			if (isset($try['status']) && $try['status'] == 1 && isset($try['trans_id']) && is_numeric($try['trans_id']) ) {
				
           			$type = 'success';
					$title = urlencode(base64_encode("title"));
					$message = urlencode(base64_encode("message"));					
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message . '/' . $try['trans_id']);
			   
           } else {
           				
           			$type = 'error';
					$title = urlencode(base64_encode("An Error Occured"));
					$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
				
            }
		}
	
	}
	
	
	function updept() {
		// Update Department
		$dept = array( 
					  'dname_r' => $this->input->post('dname'),
					  'ddesc' => $this->input->post('ddesc'),
					  'durl_r' => $this->input->post('durl'),
					  'daca' => $this->input->post('daca'),
					  'dnon' => $this->input->post('dnon')
				);
				
		$slo = array(
					'lbuild' => $this->input->post('slo_lbuild'),
					'lphone' => $this->input->post('slo_lphone'),
					'lext' => $this->input->post('slo_lext'),
					'lfax' => $this->input->post('slo_lfax')
				);
				
		$nc = array(
					'lbuild' => $this->input->post('nc_lbuild'),
					'lphone' => $this->input->post('nc_lphone'),
					'lext' => $this->input->post('nc_lext'),
					'lfax' => $this->input->post('nc_lfax')
				);
				
		$sc = array(
					'lbuild' => $this->input->post('sc_lbuild'),
					'lphone' => $this->input->post('sc_lphone'),
					'lext' => $this->input->post('sc_lext'),
					'lfax' => $this->input->post('sc_lfax')
				);
		
		$dept_id = $this->input->post('dept_id');
				
		$missing = $this->check_req($dept);
	
		
		if(!empty($missing) || !is_numeric($dept_id)){
		
			$type = 'error';
			$title = urlencode(base64_encode("An Error Occured"));
			$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
			redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
			
		} else {
			
			$update = $this->clean_req($dept);
			

			$try = $this->Adminmodel->updept($dept_id, $update, $slo, $nc, $sc);
			
			if (isset($try['status']) && $try['status'] == 1 && isset($try['trans_id']) && is_numeric($try['trans_id']) ) {
				
					// title and message can be encoded because the transaction ID will gather the results.. 
           			$type = 'success';
					$title = urlencode(base64_encode("title"));
					$message = urlencode(base64_encode("message"));					
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message . '/' . $try['trans_id']);
			   
           } else {
           				
           			$type = 'error';
					$title = urlencode(base64_encode("An Error Occured"));
					$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
				
            }
			
		}

				
	
	}
	
	
	function remdept($id) {
		// deactivate department
		$dept_id = $this->input->post('dept_id');
		$num_emp = $this->input->post('num_emp');
		
		if(is_numeric($dept_id) && $num_emp < 1){
			
			$try = $this->Adminmodel->remdept($dept_id);
			
			if (isset($try['trans_id']) && is_numeric($try['trans_id']) ) {
						// title and message can be encoded because the transaction ID will gather the results.. 
           			$type = 'success';
					$title = urlencode(base64_encode("title"));
					$message = urlencode(base64_encode("message"));					
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message . '/' . $try['trans_id']);			
			} else {
           				
           			$type = 'error';
					$title = urlencode(base64_encode("An Error Occured"));
					$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
				
            }	
							
			
		} else {
			redirect('diradmin/');
		}
	
	}
	
	function actdept($dept_id) {
		// reactivate department

		if(is_numeric($dept_id)){
			
			$try = $this->Adminmodel->actdept($dept_id);
			
			if (isset($try['trans_id']) && is_numeric($try['trans_id']) ) {
						// title and message can be encoded because the transaction ID will gather the results.. 
           			$type = 'success';
					$title = urlencode(base64_encode("title"));
					$message = urlencode(base64_encode("message"));					
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message . '/' . $try['trans_id']);			
			} else {
           				
           			$type = 'error';
					$title = urlencode(base64_encode("An Error Occured"));
					$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
				
            }	
							
			
		} else {
			redirect('diradmin/');
		}

	
	}
	
	/*  ======================
		Employee Admin Functions
		=======================
	*/
	
	function addemp() {
	  // add employee

		$emp = array( 
					  'fname_r' => $this->input->post('fname'),
					  'lname_r' => $this->input->post('lname'),
					  'email_r' => $this->input->post('email'),
					  'semail' => $this->input->post('semail'),
					  'faculty' => $this->input->post('faculty'),
					  'emp_title' => $this->input->post('emp_title')
				);
				
		$slo = array(
					'lphone' => $this->input->post('slo_lphone'),
					'sphone' => $this->input->post('slo_sphone'),
					'lext' => $this->input->post('slo_lext'),
					'slext' => $this->input->post('slo_slext'),
					'lfax' => $this->input->post('slo_lfax'),
					'sfax' => $this->input->post('slo_sfax')
				);
				
		$nc = array(
					'lphone' => $this->input->post('nc_lphone'),
					'sphone' => $this->input->post('nc_sphone'),
					'lext' => $this->input->post('nc_lext'),
					'slext' => $this->input->post('nc_slext'),
					'lfax' => $this->input->post('nc_lfax'),
					'sfax' => $this->input->post('nc_sfax')
				);
				
				
		$sc = array(
					'lphone' => $this->input->post('sc_lphone'),
					'sphone' => $this->input->post('sc_sphone'),
					'lext' => $this->input->post('sc_lext'),
					'slext' => $this->input->post('sc_slext'),
					'lfax' => $this->input->post('sc_lfax'),
					'sfax' => $this->input->post('sc_sfax')
		);

	    
	    $this->db->where('email', $emp['email_r']);
		$query = $this->db->get('emp');
		$rowcount = $query->num_rows();
		
		if($rowcount > 0){
					$type = 'error';
					$title = urlencode(base64_encode("Error Duplication"));
					$message = urlencode(base64_encode("An employee with the same email already exists in the database"));
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
		}	
		
		$dept = $this->input->post('dept');
				
		$missing = $this->check_req($emp);
		
		if(!empty($missing)){
			
		
		$type = 'error';
		$title = urlencode(base64_encode("An Error Occured"));
		$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
		redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
			
		} else {
			$add = $this->clean_req($emp);
			
			$try = $this->Adminmodel->addemp($add, $dept, $slo, $nc, $sc);
			
			if (isset($try['status']) && $try['status'] == 1 && isset($try['trans_id']) && is_numeric($try['trans_id']) ) {
				
           			$type = 'success';
					$title = urlencode(base64_encode("title"));
					$message = urlencode(base64_encode("message"));					
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message . '/' . $try['trans_id']);
			   
           } else {
           				
           			$type = 'error';
					$title = urlencode(base64_encode("An Error Occured"));
					$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
				
            }
			
			
			
		}
		
	}
	
	function upemp() {
			
		// update employee contact info, etc. 
		$emp = array( 
					  'fname_r' => $this->input->post('fname'),
					  'lname_r' => $this->input->post('lname'),
					  'email_r' => $this->input->post('email'),
					  'semail' => $this->input->post('semail'),
					  'faculty' => $this->input->post('faculty'),
					  'emp_title' => $this->input->post('emp_title')
				);
				
		$slo = array(
					'lphone' => $this->input->post('slo_lphone'),
					'sphone' => $this->input->post('slo_sphone'),
					'lext' => $this->input->post('slo_lext'),
					'slext' => $this->input->post('slo_slext'),
					'lfax' => $this->input->post('slo_lfax'),
					'sfax' => $this->input->post('slo_sfax')
				);
				
		$nc = array(
					'lphone' => $this->input->post('nc_lphone'),
					'sphone' => $this->input->post('nc_sphone'),
					'lext' => $this->input->post('nc_lext'),
					'slext' => $this->input->post('nc_slext'),
					'lfax' => $this->input->post('nc_lfax'),
					'sfax' => $this->input->post('nc_sfax')
				);
				
				
		$sc = array(
					'lphone' => $this->input->post('sc_lphone'),
					'sphone' => $this->input->post('sc_sphone'),
					'lext' => $this->input->post('sc_lext'),
					'slext' => $this->input->post('sc_slext'),
					'lfax' => $this->input->post('sc_lfax'),
					'sfax' => $this->input->post('sc_sfax')
		);
		
		
		
		
		$dept = $this->input->post('dept');
				
		$missing = $this->check_req($emp);
		
		$emp_id = $this->input->post('emp_id');
		
		if(!empty($missing) || !is_numeric($emp_id)){
		
			$type = 'error';
			$title = urlencode(base64_encode("An Error Occured"));
			$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
			redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
			
		} else {
			
			$update = $this->clean_req($emp);
			
			
			$try = $this->Adminmodel->upemp($emp_id, $update, $dept, $slo, $nc, $sc);
			
			if (isset($try['status']) && $try['status'] == 1 && isset($try['trans_id']) && is_numeric($try['trans_id']) ) {
				
					// title and message can be encoded because the transaction ID will gather the results.. 
           			$type = 'success';
					$title = urlencode(base64_encode("title"));
					$message = urlencode(base64_encode("message"));					
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message . '/' . $try['trans_id']);
			   
           } else {
           				
           			$type = 'error';
					$title = urlencode(base64_encode("An Error Occured"));
					$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
				
            }
			
		}

	}
	

	
	function rememp() {
	 // deactivate employee
	 $emp_id = $this->input->post('emp_id');
	 if(is_numeric($emp_id) && $emp_id > 0){
	 	
		$try = $this->Adminmodel->rememp($emp_id);
			
			if (isset($try['status']) && $try['status'] == 1 && isset($try['trans_id']) && is_numeric($try['trans_id']) ) {
				
					// title and message can be encoded because the transaction ID will gather the results.. 
           			$type = 'success';
					$title = urlencode(base64_encode("title"));
					$message = urlencode(base64_encode("message"));					
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message . '/' . $try['trans_id']);
			   
           } else {
           				
           			$type = 'error';
					$title = urlencode(base64_encode("An Error Occured"));
					$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
				
            }
	 		
	 } else {
			redirect('diradmin/');
		}
	 

	}
	function actemp($emp_id) {
		// reactivate employee

	 if(is_numeric($emp_id) && $emp_id > 0){
	 	
		$try = $this->Adminmodel->actemp($emp_id);
			
			if (isset($try['status']) && $try['status'] == 1 && isset($try['trans_id']) && is_numeric($try['trans_id']) ) {
				
					// title and message can be encoded because the transaction ID will gather the results.. 
           			$type = 'success';
					$title = urlencode(base64_encode("title"));
					$message = urlencode(base64_encode("message"));					
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message . '/' . $try['trans_id']);
			   
           } else {
           				
           			$type = 'error';
					$title = urlencode(base64_encode("An Error Occured"));
					$message = urlencode(base64_encode("The form was submited with missing fields"));
			 
					redirect('diradmin/notification/' . $type . '/' . $title . '/' . $message);
				
            }
	 		
	 } else {
			redirect('diradmin/');
		}
	
	}
	

	function check_req($arr){
		// check required
		$errors = array();
		foreach ($arr as $key => $value) {
			 if(strpos($key,'_r') !== false && $value == ""){
			 	$newkey = str_replace('_r', '', $key);
				$error[$newkey] = 1;
			 }
		}
		
		return $errors;
	
	}
	
	function clean_req($arr){
		// removes the required _r from the key.. 
		$new = array();
		foreach ($arr as $key => $value) {
			 if(strpos($key,'_r') !== false){
				$newkey = str_replace('_r', '', $key);
				$new[$newkey] = $value;
			 } else {
			 	$new[$key] = $value;
			 }
		}
		return $new;
		
	}
	
}