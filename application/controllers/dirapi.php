<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dirapi extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
		$this->load->model('Adminmodel');
		$this->load->model('Dirmodel');
		$this->response = array('status' => 0,
            'message' => 'Unexpected error.',
            'errors' => array());
    }
	
	public function deptlist($type = ''){
		// send back a list of all departments. 
		
		$this->db->select('id, dname, durl');
		$this->db->where('dactive', 1);
		$this->db->order_by('dname');
		$query = $this->db->get('dept');
		$result = $query->result_array();
		
		if($type == ''){
		$return = json_encode($result);
					
			
			echo $return;
			
		} else if($type == 'xml'){
			$this->load->helper('xml');
			$count = count($result);
			if ($count > 0) {
				
        		$return = new SimpleXMLElement('<xml/>');
      		  	$depts = $return->addChild('departments');

       		 	for($i=0; $i<$count; $i++){
            		$dept = $depts->addChild('dept');
            			$dept->addChild('id', $result[$i]['id']);
						$dname = xml_convert($result[$i]['dname']);
						$durl = xml_convert($result[$i]['durl']);
						$dept->addChild('dname', $dname);
						$dept->addChild('durl', $durl);
				
				}
				Header('Content-type: text/xml');
			    print($return->asXML());
			}

    	} else if($type == 'alpha'){
    			
    		$count = count($result);
			if ($count > 0) {
				
				$alpha = array();
				
       		 	for($i=0; $i<$count; $i++){
       		 		$az = strtolower($result[$i]['dname'][0]);
					$alpha[$az][] = array( 
								 'id' => $result[$i]['id'],
								 'dname' => $result[$i]['dname'],
								 'durl' => $result[$i]['durl']
								);
												
				}
				$total = 0;
				$cols = 75;
				$left = array();
				$center = array();
				$right = array();
				foreach($alpha as $key=>$value){
						
						$num = count($value);
						$total += $num;	
						if($total <= $cols){
							$left[$key] = $value;
							
						} else if($total > $cols && $total <= 120){
							$center[$key] = $value;
							
						} else {
							$right[$key] = $value;
							
						}
				}

			$data = array('left' => $left, 'center'=> $center, 'right'=> $right);
		  
	 		$this->load->view('api/front', $data);	
				
						
    		}
			
		
		}

	}

	public function dept(){
			if(is_numeric($this->uri->segment(3))){
				$id = $this->uri->segment(3);
				$dept = $this->Dirmodel->dept($id, '', false, '');	
				$slo = $this->buildpretty($this->Dirmodel->deptloc($id, 1), 0, 'slo', 'SLO Campus');
				$nc = $this->buildpretty($this->Dirmodel->deptloc($id, 2), 0, 'nc', 'NC Campus');
				$sc = $this->buildpretty($this->Dirmodel->deptloc($id, 3), 0, 'sc', 'SC Campus');
				$data = array('dept' => $dept, 'slo'=> $slo, 'nc' => $nc, 'sc' => $sc);
		  
	 			$this->load->view('api/publicdept', $data);	
			}

	}
	
	public function emp(){
		if(is_numeric($this->uri->segment(3))){	
			$emp = $this->Dirmodel->emp($this->uri->segment(3));
			$emp[0]['slo'] = $this->buildemploc($emp[0]['emploc']['slo'], 'SLO Campus');
    		$emp[0]['nc'] = $this->buildemploc($emp[0]['emploc']['nc'], 'NC Campus');
    		$emp[0]['sc'] = $this->buildemploc($emp[0]['emploc']['sc'], 'SC Campus');
			$emp[0]['depts'] = $this->Dirmodel->empdeptlist($emp[0]['id']);
			$data = array('emp' => $emp);
		  
	 		$this->load->view('api/singleemp', $data);	
		}
	}
	
	public function empdept(){

		// Get all employees 
		if(is_numeric($this->uri->segment(3))){
			
			$emps = $this->Dirmodel->emp('', '', $this->uri->segment(3));
			
			$count = count($emps);
			for($i=0; $i<$count; $i++){
							
				    $emps[$i]['slo'] = $this->buildemploc($emps[$i]['emploc']['slo'], 'SLO Campus');
    				$emps[$i]['nc'] = $this->buildemploc($emps[$i]['emploc']['nc'], 'NC Campus');
    				$emps[$i]['sc'] = $this->buildemploc($emps[$i]['emploc']['sc'], 'SC Campus');
								
			}
			$data['emp'] = $emps;
			$data['dept'] = $this->Dirmodel->dept($this->uri->segment(3));
		
		 $this->load->view('api/publicemp', $data);
		}
	}
	
	
	
	public function campusmap(){
		$campus = $this->uri->segment(3);
		$building = $this->uri->segment(4);
		
		$map = '';
		if($building != '' && $campus == 'slo'){
			
			$building = ltrim($building, 'b');
			
			// get only whole building (not 3412)
			$ft = substr($building, 0, 2) . '00';
			// areas with multiple buidings:
			if($ft == '3300' || $ft == '3400'){
				$map = '3300-3400';
			} else if($ft == '3100' || $ft == '3200' ){
				$map = '3100-3200';
			} else if($ft == '7400' || $ft == '7500'){
				$map = '7400-7500';
			} else {
				$map = $ft;
			}
		}
		if($building != '' && $campus == 'nc'){
			$ft = substr($building, 0, 3) . '00';
			
			// areas with multiple buidings:

			if($ft == 'N3200' || $ft == 'N3100'){
				$map = 'N3100-N3200';
			} else {
				$map = $ft;
			} 
			
					
		}
		$data = array('campus' => $campus, 'location' => $map);
		$this->load->view('campusmaps/campusmaps', $data);	
		
	}
	
	public function usersearch(){
		$q =  $this->input->get('q', true);
		$callback = $this->input->get('callback', true);
		$drop_items = $this->empsearchdb($q, $callback);
		$return_array = array('emp' => $drop_items);
		$return_emp =  $callback . '('.json_encode($return_array) . ')';
		echo $return_emp;
		
		
	}

	public function deptsearch(){
		$q =  $this->input->get('q', true);
		$callback = $this->input->get('callback', true);
		$drop_items = $this->deptsearchdb($q, $callback);
		$return_array = array('dept' => $drop_items);		
		$return =  $callback . '('.json_encode($return_array) . ')';
		echo $return;

	}

	public function allsearch(){
		$q =  $this->input->get('q', true);
		$callback = $this->input->get('callback', true);
		
		if(is_numeric($q)){ // reverse phone number lookup
		
			$drop_items = $this->reversephone($q, $callback);	
			
		} else {
			$emp_items = $this->empsearchdb($q, $callback);	
			$dept_items = $this->deptsearchdb($q, $callback);

		// combine two arrays 
		$drop_items = array_merge($emp_items, $dept_items);
		}
		
		$return_array = array('empdept' => $drop_items);
		
		$return_emp =  $callback . '('.json_encode($return_array) . ')';
		  echo $return_emp;
				
		
	}
	
	private function deptsearchdb($q, $callback){
				
			$drop_items = array();
		 	if($q != '' && strlen($q) > 2 && $callback != ''){
			
			$dept = $this->Dirmodel->dept("", $q, false, '');	
			$count = count($dept);
			
			for($i=0;$i<$count;$i++){
				$drop_items[$i]['alpha'] = $dept[$i]['dname'];
				$drop_items[$i]['type'] = 'dept';
				$drop_items[$i]['dname'] = $dept[$i]['dname'];
				$drop_items[$i]['id'] = $dept[$i]['id'];
				$slo = $this->Dirmodel->deptloc($dept[$i]['id'], 1);
				$nc = $this->Dirmodel->deptloc($dept[$i]['id'], 2);
				$sc = $this->Dirmodel->deptloc($dept[$i]['id'], 3);
				$drop_items[$i]['slo'] = $this->autodeptformat($slo[0]['lphone'], $slo[0]['lext'], $slo[0]['lbuild'], 'SLO Campus');
				$drop_items[$i]['nc'] = $this->autodeptformat($nc[0]['lphone'], $nc[0]['lext'], $nc[0]['lbuild'], 'NC Campus');
				$drop_items[$i]['sc'] = $this->autodeptformat($sc[0]['lphone'], $sc[0]['lext'], $sc[0]['lbuild'], 'SC Campus');
			}

			}
			
			return $drop_items;
	}
	 
	private function empsearchdb($q, $callback){
		
		$drop_items = array();
		if($q != '' && strlen($q) > 2 && $callback != ''){
			$emp = $this->Dirmodel->emp("", $q, "", "", true);

			$count = count($emp);
			
		for($i=0;$i<$count;$i++) {
			
			$sloloc = $this->autoempformat($emp[$i]['emploc']['slo'][0]['lphone'], $emp[$i]['emploc']['slo'][0]['sphone'], $emp[$i]['emploc']['slo'][0]['slext'], $emp[$i]['emploc']['slo'][0]['lext'], 'SLO Campus');
			$ncloc = $this->autoempformat($emp[$i]['emploc']['nc'][0]['lphone'], $emp[$i]['emploc']['nc'][0]['sphone'], $emp[$i]['emploc']['nc'][0]['slext'], $emp[$i]['emploc']['nc'][0]['lext'], 'NC Campus');
			
			$drop_items[$i]['alpha'] = $emp[$i]['lname'];
			$drop_items[$i]['type'] = 'emp';
			$drop_items[$i]['id'] = $emp[$i]['id']; 
			$drop_items[$i]['name'] = $emp[$i]['fname'] . " " . $emp[$i]['lname'];
			$drop_items[$i]['depts'] = $this->Dirmodel->empdeptlist($emp[$i]['id']);
			/* Titles are ugly so we will leave them out until they are fixed.. 
			if(trim($emp[$i]['emp_title']) != trim($drop_items[$i]['depts'])){
				$drop_items[$i]['emp_title'] = $emp[$i]['emp_title'];
			}
			 */
			if($sloloc != ''){
				$drop_items[$i]['slo'] = $sloloc;
			}
			
			if($ncloc != ''){
				$drop_items[$i]['nc'] = $ncloc;
			}

		}

		}
		
		return $drop_items;
	
	}

	private function reversephone($q, $callback){
			
		
		$all_drop = array();
		
		if($q != '' && strlen($q) > 2 && $callback != ''){
			$emp = $this->Dirmodel->reversephone($q, 'emp');
			$drop_items = array();
			$dep_items = array();
			$count = count($emp);
			
			for($i=0;$i<$count;$i++) {
				
				$sloloc = $this->autoempformat($emp[$i]['emploc']['slo'][0]['lphone'], $emp[$i]['emploc']['slo'][0]['sphone'], $emp[$i]['emploc']['slo'][0]['slext'], $emp[$i]['emploc']['slo'][0]['lext'], 'SLO Campus');
				$ncloc = $this->autoempformat($emp[$i]['emploc']['nc'][0]['lphone'], $emp[$i]['emploc']['nc'][0]['sphone'], $emp[$i]['emploc']['nc'][0]['slext'], $emp[$i]['emploc']['nc'][0]['lext'], 'NC Campus');
			
				$drop_items[$i]['alpha'] = $emp[$i]['lname'];
				$drop_items[$i]['type'] = 'emp';
				$drop_items[$i]['id'] = $emp[$i]['id']; 
				$drop_items[$i]['name'] = $emp[$i]['fname'] . " " . $emp[$i]['lname'];
				$drop_items[$i]['depts'] = $this->Dirmodel->empdeptlist($emp[$i]['id']);
			
				if($sloloc != ''){
					$drop_items[$i]['slo'] = $sloloc;
				}
			
				if($ncloc != ''){
					$drop_items[$i]['nc'] = $ncloc;
				}
			

			} // end loop
		
			// check department extension
			
			$dept = $this->Dirmodel->reversephone($q, 'dept');
			$count = count($dept);
			
			for($i=0;$i<$count;$i++){
				$dep_items[$i]['alpha'] = $dept[$i]['dname'];
				$dep_items[$i]['type'] = 'dept';
				$dep_items[$i]['dname'] = $dept[$i]['dname'];
				$dep_items[$i]['id'] = $dept[$i]['id'];
				$slo = $this->Dirmodel->deptloc($dept[$i]['id'], 1);
				$nc = $this->Dirmodel->deptloc($dept[$i]['id'], 2);
				$sc = $this->Dirmodel->deptloc($dept[$i]['id'], 3);
				$dep_items[$i]['slo'] = $this->autodeptformat($slo[0]['lphone'], $slo[0]['lext'], $slo[0]['lbuild'], 'SLO Campus');
				$dep_items[$i]['nc'] = $this->autodeptformat($nc[0]['lphone'], $nc[0]['lext'], $nc[0]['lbuild'], 'NC Campus');
				$dep_items[$i]['sc'] = $this->autodeptformat($sc[0]['lphone'], $sc[0]['lext'], $sc[0]['lbuild'], 'SC Campus');
			}

			$all_drop = array_merge($drop_items, $dep_items);

		} 
		
		return $all_drop;		 
	}

	/* Formatting Functions */

	private function buildpretty($loc, $i, $lockey, $locname){
				$build = '';
				if($loc[0]['lphone'] != '' || $loc[0]['lext'] != '' || $loc[0]['lbuild'] != ''){
					$build .= $locname;
					if($loc[0]['lphone'] != ''){
						$build .= " - " . $loc[0]['lphone'];
					}
					if($loc[0]['lext'] != ''){
						$build .= " Ext:" . $loc[0]['lext'];
					}
					if($loc[0]['lbuild'] != ''){
						$title = "Building: " . $loc[0]['lbuild'] . " <i class='fa fa-map-marker'></i>";
						$href = 'dirapi/campusmap/'. $lockey . '/' . $loc[0]['lbuild'];
						$link =  " " . anchor($href, $title );
						$build .= $link; 
					}
					if($loc[0]['lfax'] != ''){
						$build .= " Fax: " . $loc[0]['lfax'];
					}
					
				}
				$return[$i][$lockey] = $build;
				return $return;
	}
	
	private function buildemploc($loc, $locname){
				$build = '';
						
				if($loc[0]['lphone'] != '' || $loc[0]['lext'] != '' || $loc[0]['lfax'] ){
					$build .= $locname;
					if($loc[0]['lphone'] != ''){
						$build .= " - " . $loc[0]['lphone'];
					}
					if($loc[0]['lext'] != ''){
						$build .= " Ext:" . $loc[0]['lext'];
					}

					if($loc[0]['lfax'] != ''){
						$build .= " Fax: " . $loc[0]['lfax'];
					}
					
					
				}
				return $build;
	}
	
	private function autodeptformat($lphone, $lext, $lbuild, $campus){
				
			$return = '';
			
			if($lphone != '' || $lext != '' || $lbuild != ''){
					$return .=  $campus . ':';
					if($lphone != ''){
						$return .= ' ' . $lphone;
					}
					if($lext != ''){
						$return .= ' Ext:' . $lext;
					}
					if($lbuild != ''){
						$return .= ' Building: ' . $lbuild;
					}
					
				}
			
				return $return;
	}
	
	private function autoempformat($lphone, $sphone, $slext, $lext, $campus){
		
		$return = '';
		
		if($lphone != '' ||  $lext != '' ){
					
				if($sphone == 1){
					$addslophone = true;
				} else {
					$addslophone = false;
				}
				if($slext == 1){
					$addsloext = true;
				} else {
					$addsloext = false;
				}
				if($addslophone || $addsloext){
					$return .= $campus . ':';	
				}
				if($addslophone && $lphone != ''){
					$return .= ' ' . $lphone;
				}
				
				if($addsloext && $lext != ''){
					$return .= ' Ext ' . $lext;
				}
				
			}
		return $return;
	}
	
	function deptempsort($x, $y){
		if($x['alpha'] == $y['alpha']){
			return 0;
		} else if($x['alpha'] < $y['alpha']){
			return -1;
		} else {
			return 1;
		}
	}
	

	 
	
}