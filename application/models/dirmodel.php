<?php
class Dirmodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function dept($id = "", $search = "", $emp = false, $type = ''){
				
				// get all departments by either id or search
				// $allq =  "SELECT * FROM dept WHERE dactive = 1";
				
				$allq = "SELECT dept.id, dept.dname, dept.ddesc, dept.durl, dept.daca, dept.dnon, dactive, emp_count.numemp 
			 	FROM dept 
				 LEFT OUTER JOIN
					( 
       					SELECT dept_id, COUNT(deptemp.id) as numemp FROM deptemp GROUP BY dept_id
    				) AS emp_count
			 	ON dept.id = emp_count.dept_id
			 	WHERE dactive = 1";
	
				if($id != "" && is_numeric($id)){
					// select by ID
					$allq .= " AND dept.id = ? ";
					$dquery =  $this->db->query($allq, array($id));					
				} else if ($search != "") {
					// search by department name
					$allq .= " AND dept.dname LIKE '%".$this->db->escape_like_str($search)."%' ";
					$dquery =  $this->db->query($allq);
				} else {
					// both are null get all departments
					$allq .= "";
					// check for academic or non-academic departments
					if($type == 'aca'){
						$allq .= ' AND dept.daca = 1' ;
					} else if($type == 'naca'){
						$allq .= ' AND dept.dnon = 1' ;
					}
					$dquery =  $this->db->query($allq);
				}

				$deptresult = $dquery->result_array();
				
				if($emp) {
					// get all the employees for this department
					
					for($i=0; $i < $dquery->num_rows(); $i++){
						$deptresult[$i]['emp'] = $this->emp("", "", $deptresult[$i]['id']);
					}
					
									
				}

				return $deptresult;
									
	
	}

	function emp($id = "", $search = "", $deptid = "", $locid = "", $getloc = true){
				
				// Select employees by department
				if($deptid != "" && is_numeric($deptid)){
				
					$sql = "SELECT emp.id, emp.fname, emp.lname, emp.emp_title, emp.email, emp.faculty, emp.semail
							FROM emp, deptemp, dept
							WHERE emp.id = deptemp.emp_id
							AND deptemp.dept_id = dept.id
							AND emp.active = 1
							AND dept.id = ? "; 
							
					$empquery =  $this->db->query($sql, array($deptid));
				
				// Select employees by location	
				} else if($locid != "") {
					
					$sql = "SELECT emp.fname, emp.lname, emp.email, emp.emp_title, emploc.lphone, emploc.lext
						    FROM emp, emploc
				            WHERE emp.id = emploc.emp_id
                            AND emploc.loc_id = ? "; 
							
					$empquery =  $this->db->query($sql, array($locid));
				
				// select individual employees by id or search by name
				} else {
			
					$sql = "SELECT emp.id, emp.fname, emp.lname, emp.emp_title, emp.email, emp.bannerid, emp.faculty, emp.semail FROM emp WHERE emp.active = 1 ";
				
					if($id != ""){
						// select by ID
						$sql .= " AND emp.id = ? ";
						$empquery =  $this->db->query($sql, array($id));	
					} else if ($search != "") {
						// search by department name
						$sql .= " AND concat(emp.fname, ' ', emp.lname) like '%".$this->db->escape_like_str($search)."%'";
						$empquery =  $this->db->query($sql);
					} else {
						// both are null get all employees
						$sql .= "";
						$empquery =  $this->db->query($sql);
					}
				
				}
				
				// get all the employee contact
					$empresult = $empquery->result_array();
				
				if($getloc){
					
					for($i=0; $i < $empquery->num_rows(); $i++){
						$empresult[$i]['emploc']['slo'] = $this->emploc($empresult[$i]['id'], 1);
						$empresult[$i]['emploc']['nc'] = $this->emploc($empresult[$i]['id'], 2);
						$empresult[$i]['emploc']['sc'] = $this->emploc($empresult[$i]['id'], 3);
					}
				}
				
				return $empresult;			
	
	}
	
	function emploc($emp_id, $loc_id = ''){
			
				$loc = "SELECT emploc.lphone, emploc.sphone, emploc.lfax, emploc.sfax, emploc.lext, emploc.slext, loc.locname, loc.id
				FROM emploc, emp, loc
				WHERE emploc.emp_id = emp.id
				AND loc.id = emploc.loc_id
				AND emp.id = ? ";
				
				if(is_numeric($loc_id)){
					$loc .= ' AND loc.id = ' . $loc_id ;
				}
				 
				
				$emplocq =  $this->db->query($loc, array($emp_id));
				
				$result = $emplocq->result_array();
				
				if(empty($result)){
					$result = array( 0 => array ( 'lphone' => '', 'sphone' => '', 'lfax' => '', 'sfax' => '', 'lext' => '', 'slext' => '', 'locname' => '' ));
				}
				
				return $result;
	
	}
	
	function deptloc($dept_id, $loc_id = ''){
					
				$loc = "SELECT deptloc.lphone, deptloc.lfax, deptloc.lext, deptloc.lbuild, loc.locname
				FROM deptloc, dept, loc
				WHERE deptloc.dept_id = dept.id
				AND loc.id = deptloc.loc_id
				AND dept.id = ? ";
				
				if(is_numeric($loc_id)){
					$loc .= ' AND loc.id = ' . $loc_id ;
				}
				
				 
				$deptq =  $this->db->query($loc, array($dept_id));
				$result = $deptq->result_array();
				
				if(empty($result)){
					$result = array( 0 => array ( 'lphone' => '', 'lfax' => '', 'lext' => '', 'lbuild' => '', 'locname' => '' ));
				}
				
				return $result;
	}
	
	function deptemp($emp_id){
		
		// get all departments and mark departments employee has access to
		$q = 'SELECT dept.id, dept.dname, aa.id as mem 
			 FROM dept 
			 LEFT OUTER JOIN
				( 
       				SELECT deptemp.id, deptemp.dept_id
       				FROM deptemp
       				WHERE deptemp.emp_id = ?
    			) AS aa
			 ON dept.id = aa.dept_id
			 WHERE dept.dactive = 1
			 ORDER BY dept.dname ASC';
			 
		$query =  $this->db->query($q, array($emp_id));	
		$result = $query->result_array();
		
		return $result;	
		
	}
	
	function loc($id = "", $search = "", $emp = ""){
			
				// get all departments by either id or search
				$allq =  "SELECT loc.id, loc.locname FROM loc ";
				if($id != ""){
					// select by ID
					$allq .= " WHERE loc.id = ? ";
					$lquery =  $this->db->query($allq, array($id));					
				} else if ($search != "") {
					// search by location name
					$allq .= " WHERE loc.locname LIKE '%".$this->db->escape_like_str($search)."%' ";
					$lquery =  $this->db->query($allq);
				} else {
					// get all locations
					$allq .= "";
					$lquery =  $this->db->query($allq);
				}
				
				$locresult = $lquery->result_array();
				
				if($emp) {
					
					// get all the employees for this location
					for($i=0; $i < $locquery->num_rows(); $i++){
						$locresult[$i]['emp'] = $this->emp("", "", "", $locresult[$i]['id']);
					}				
				}	
				
				return $locresult;
	
	}
	
	function empdeptlist($emp_id){
		$q = "SELECT dept.dname 
		 	  	FROM dept, deptemp
		 		WHERE dept.id = deptemp.dept_id
		 		AND deptemp.emp_id = ? ";
				$query =  $this->db->query($q, array($emp_id));
				$depts = '';
				$result = $query->result_array();
				$count = count($result);
				for($i=0;$i<$count; $i++){
					$depts .= $result[$i]['dname'];
					if($i != ($count-1)){
						$depts .= ", ";
					}
				}	
				return  $depts;
	}
	
	function reversephone($p, $type){
		
			if($type == 'emp'){
				 
				$q =  "SELECT emp.id, emp.fname, emp.lname, emp.emp_title, emp.email, emp.faculty, emp.semail
						FROM emploc, emp
						WHERE emploc.emp_id = emp.id
						AND emploc.lext LIKE '%".$this->db->escape_like_str($p)."%' ";
				
				$empquery = $this->db->query($q);	
				$empresult = $empquery->result_array();
			
			
				
				for($i=0; $i < $empquery->num_rows(); $i++){
						$empresult[$i]['emploc']['slo'] = $this->emploc($empresult[$i]['id'], 1);
						$empresult[$i]['emploc']['nc'] = $this->emploc($empresult[$i]['id'], 2);
						$empresult[$i]['emploc']['sc'] = $this->emploc($empresult[$i]['id'], 3);
				}
		
				
				return $empresult;
				
			} else {
				
				
				$q ="SELECT dept.id, dept.dname, dept.ddesc, dept.durl, dept.daca, dept.dnon, dactive
						FROM deptloc, dept
						WHERE deptloc.dept_id = dept.id
					    AND deptloc.lext LIKE '%".$this->db->escape_like_str($p)."%' ";
				
				$deptquery = $this->db->query($q);	
				$deptresult = $deptquery->result_array();
						
				return $deptresult;
			}		
	
		
	}
	

}