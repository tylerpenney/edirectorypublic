<?php
class Adminmodel extends CI_Model {

	var $user_id;
	var $err = array();
	var $ok = array();
	var $newid;
	var $update;

    function __construct()
    {
        parent::__construct();
		
		$this->user_id = $this->session->userdata('id');
		$this->err['status'] = 0;
		$this->err['desc'] = "Unknown Error Occurred";
		$this->ok['status'] = 1;
		$this->ok['desc'] = "Success";
		$this->newid = 0;
		$this->update = 0;
    }
	
	/*  ======================
		Department Admin Functions
	    =======================
	*/
	
	function adddept($dept = array(), $slo = array(), $nc = array(), $sc = array()){
		// Add Department

		if(!empty($dept)){
			$this->db->insert('dept', $dept);
			$newid = $this->db->insert_id();
		} else {
			$this->err['desc'] = "Could not enter new department information ";
			return $this->err;
		}
		if($newid){
			
			// Start new transaction 
			$trans = 'Added New Department: ' . $dept['dname'];
			$trans_id = $this->transaction($trans, 'new-dept');
			
			$ids = " AND id = " . $newid;
			$this->buildlog('Added Department ', 'dept', $ids, $trans_id);
			
			$this->checkadddeptloc($newid, 1, $slo, $trans_id);
			$this->checkadddeptloc($newid, 2, $nc, $trans_id);
			$this->checkadddeptloc($newid, 3, $sc, $trans_id);
			
			$return = array( 'status' => 1, 'trans_id' => $trans_id );
			
			return $return;
			
			
		} else {
			return $this->err;
		}
		
	
	}
	
	function updept($id, $dept = array(), $slo = array(), $nc = array(), $sc = array()) {
		// update department description info, etc. 
		
			// Start new transaction 
			$trans = 'Updated Department: ' . $dept['dname'];
			$trans_id = $this->transaction($trans, 'up-dept');
		
		if(is_numeric($id) && !empty($dept)){
				
			// lets find out if anything has changed. 
			$this->db->where('id', $id);
			$query = $this->db->get('dept');
			$result = $query->result_array();
			$current = array(
						'dname' => $result[0]['dname'],
						'durl' => $result[0]['durl'],
						'daca' => $result[0]['daca'],
						'dnon' => $result[0]['dnon']						
						);
			
			if($dept['daca'] == ''){
				$dept['daca'] = 0;
			}
			
			if($dept['dnon'] == ''){
				$dept['dnon'] = 0;
			}
			
			// remove from department 
			$dept_nodesc = $dept;
			unset($dept_nodesc['ddesc']);
			
			$db_str = strlen(trim($dept['ddesc']));
			$current_str = strlen(trim($result[0]['ddesc']));
						
			$change_left = array_diff($dept_nodesc, $current);
			$change_right = array_diff($current, $dept_nodesc);
			$change_all = array_merge($change_left, $change_right);	
			
			/* $test = array('loggy_title' => 'Change', 'loggy' => print_r($change_all, true));
			$this->db->insert('temp_log', $test);	*/			
			
			if(!empty($change_all) || $current_str != $db_str){		
			
				$this->db->where('id', $id);
				$update = $this->db->update('dept', $dept);
			
				$ids = " AND id = " . $id;
				$this->buildlog('Updated Department Info', 'dept', $ids, $trans_id);
			
			}
			
			
		} else {
			$this->err['desc'] = "Could not update department information ";
			return $this->err;
		}
		
		
			$this->checkadddeptloc($id, 1, $slo, $trans_id);
			$this->checkadddeptloc($id, 2, $nc, $trans_id);
			$this->checkadddeptloc($id, 3, $sc, $trans_id);
		
			$return = array( 'status' => 1, 'trans_id' => $trans_id );
		
			return $return;		
		
	
	}
	
	function adddeptloc($deptloc = array(), $trans_id) {
		// add department location contact info. 
		
		if(!empty($deptloc)){
			$this->db->insert('deptloc', $deptloc);
			$newid = $this->db->insert_id();
		} else {
			$this->err['desc'] = "Could not enter new department contact information ";
			return $this->err;
		}
		if($newid){

			$ids = " AND id = " . $newid;
			$this->buildlog('Added Department Contact ', 'deptloc', $ids, $trans_id);
			return $this->ok;
		} else {
			return $this->err;
		}
		
	
	}
	
	function updeptloc($dept_id, $loc_id, $deptloc = array(), $trans_id) {
			
		// update department location contact info
		if(is_numeric($dept_id) && is_numeric($loc_id) && !empty($deptloc)){
				
			// check for update	
			
			$this->db->where('dept_id', $dept_id);
			$this->db->where('loc_id', $loc_id);
			$query = $this->db->get('deptloc');
			$result = $query->result_array();
			
			$current = array(
						'lphone' => $result[0]['lphone'],
						'lfax' => $result[0]['lfax'],
						'lext' => $result[0]['lext'],
						'lbuild' => $result[0]['lbuild']					
						);
						
			$change_left = array_diff($deptloc, $current);
			$change_right = array_diff($current, $deptloc);
			$change_all = array_merge($change_left, $change_right);	
			
			if(!empty($change_all)){
					
				$this->db->where('dept_id', $dept_id);
				$this->db->where('loc_id', $loc_id);
				$update = $this->db->update('deptloc', $deptloc);
			
				if($update){
					$ids = " AND dept_id = " . $dept_id . " AND loc_id = " . $loc_id;
					$this->buildlog('Updated Dept Contact Info', 'deptloc', $ids, $trans_id);
					return $this->ok;
				}
			
			}
			
		} else {
			$this->err['desc'] = "Could not update department contact information ";
			return $this->err;
		}
		
	} 
	
	function remdept($id) {
		// deactivate department
		if(is_numeric($id)) {
			
			
			// count # of employees associated with department
			$this->db->where('dept_id', $id);
			$countemp = $this->db->count_all_results('deptemp');
			
			if($countemp < 1){
				$data = array('dactive' => 0);
				$this->db->where('id', $id);
				$update = $this->db->update('dept', $data);
			}
			
		} else {
			$this->err['desc'] = "Could not deactivate department";
			return $this->err;
		} 
		
		if($update){
			// Start new transaction 
			$this->db->where('id', $id);
			$query = $this->db->get('dept');
			$result = $query->result_array();
			
			$trans = 'Removed Department: ' . $result[0]['dname'];
			$trans_id = $this->transaction($trans, 'rem-dept');
			$return = array( 'status' => 1, 'trans_id' => $trans_id );
			return $return;		
			
		} else {
			return $this->err;
		}
	
	}
	
	function actdept($id) {
		// reactivate department
		if(is_numeric($id)) {
			$data = array('dactive' => 1);
			$this->db->where('id', $id);
			$update = $this->db->update('dept', $data);
		} else {
			$this->err['desc'] = "Could not reactivate department";
			return $this->err;
		} 
		
		if($update){
			// Start new transaction 
			$this->db->where('id', $id);
			$query = $this->db->get('dept');
			$result = $query->result_array();
			
			$trans = 'Reactivated Department: ' . $result[0]['dname'];
			$trans_id = $this->transaction($trans, 'act-dept');
			$return = array( 'status' => 1, 'trans_id' => $trans_id );
			return $return;		
		} else {
			return $this->err;
		}
	
	}
	
	/*  ======================
		Employee Admin Functions
		=======================
	*/
	
	function addemp($emp = array(), $depts = array(), $slo = array(), $nc = array(), $sc = array()) {
	  // add employee
		if(!empty($emp)){
			$emp['activedate'] = date("Y-m-d H:i:s");
			$emp['active'] = 1;
			$this->db->insert('emp', $emp);
			$newid = $this->db->insert_id();
		} else {
			$this->err['desc'] = "Could not add new employee";
			return $this->err;
		}
		if($newid){
			
			// start transaction for new employee
			$trans = 'Added New Employee: ' . $emp['fname'] . ' ' . $emp['lname'];
			$trans_id = $this->transaction($trans, 'new-emp');
	
			$ids = " AND id = " . $newid;
			$this->buildlog('Added Employee ', 'emp', $ids, $trans_id);		
			
			// check and add location contact, if exists, update - true/false to check all values for update
			$this->checkaddloc($newid, 1, $slo, false, $trans_id);
			$this->checkaddloc($newid, 2, $nc, false, $trans_id);
			$this->checkaddloc($newid, 3, $sc, false, $trans_id);
			
			// check if any departments are added or removed
			$this->upempdept($depts, $newid, $trans_id);
			
			$return = array( 'status' => 1, 'trans_id' => $trans_id );
			
			return $return;
			
		} else {
			return $this->err;
		}
	}
	
	function upemp($id, $emp = array(), $depts = array(), $slo = array(), $nc = array(), $sc = array()) {
				
		// start transaction for employee update. 
			$trans = 'Updated Employee: ' . $emp['fname'] . ' ' . $emp['lname'];
			$trans_id = $this->transaction($trans, 'up-emp');	

		// update employee contact info, etc. 
		if(is_numeric($id) && !empty($emp)){
			
			$this->db->where('id', $id);
			$update = $this->db->update('emp', $emp);
			
			$ids = " AND id = " . $id;
			$this->buildlog('Updated Employee Info', 'emp', $ids, $trans_id);
			
		} else {
			$this->err['desc'] = "Could not update employee info ";
			return $this->err;
		}
		
	
		// check and add location contact, if exists, update - true/false to check all values for update 
		$this->checkaddloc($id, 1, $slo, false, $trans_id);
		$this->checkaddloc($id, 2, $nc, false, $trans_id);
		$this->checkaddloc($id, 3, $sc, false, $trans_id);
		
		// check if any departments are added or removed
		$this->upempdept($depts, $id, $trans_id);
		
		$return = array( 'status' => 1, 'trans_id' => $trans_id );
		
		return $return;
			

	}
	
	private function addemploc($empcon = array(), $trans_id) {
		// add employee location contact
		if(!empty($empcon)){
			
			// add "show" bool to values that have been added on initial add. 
			if($empcon['lphone'] != ''){
			    $empcon['sphone'] = 1;
			}
			if($empcon['lext'] != ''){
			   $empcon['slext'] = 1;
			}
			if($empcon['lfax'] != '') {
				$empcon['sfax'] = 1;
			}
			
			if($empcon['lphone'] == ''){
				$empcon['lphone'] = null;
			}
			
			if($empcon['lext'] == ''){
				$empcon['lext'] = null;
			}
						
			if($empcon['lfax'] == ''){
				$empcon['lfax'] = null;
			}
			
			$this->db->insert('emploc', $empcon);
			$newid = $this->db->insert_id();
			
		} else {
			$this->err['desc'] = "Could not add new user contact information ";
			return $this->err;
		}
		if($newid){
			$ids = " AND id = " . $newid;
			$this->buildlog('Employee Contact Added ', 'emploc', $ids, $trans_id);
			return $newid;
		} else {
			return $this->err;
		}	
	}
	
	private function upemploc($emp_id, $loc_id, $emploc = array(), $trans_id) {
		// update employee location contact info
		if(is_numeric($emp_id) && !empty($emploc)){
			
			// check for differences
			$this->db->where('emp_id', $emp_id);
			$this->db->where('loc_id', $loc_id);
			$query = $this->db->get('emploc');
			$result = $query->result_array();
			
			$current = array(
						'lphone' => $result[0]['lphone'],
						'sphone' => $result[0]['sphone'],
						'lext' => $result[0]['lext'],
						'slext' => $result[0]['slext'],
						'lfax' => $result[0]['lfax'],
						'sfax' => $result[0]['sfax']						
						);
											
			if($emploc['sphone'] == ''){
				$emploc['sphone'] = 0;
			}
			
			if($emploc['slext'] == ''){
				$emploc['slext'] = 0;
			}	
			
			if($emploc['sfax'] == ''){
				$emploc['sfax'] = 0;
			}		
						
			$change_left = array_diff($emploc, $current);
			$change_right = array_diff($current, $emploc);
			$change_all = array_merge($change_left, $change_right);
			

			if(!empty($change_all)){ // difference in results.. update the db and record update.  
			
				$this->db->where('emp_id', $emp_id);
				$this->db->where('loc_id', $loc_id);
				$update = $this->db->update('emploc', $emploc);
			
			
				if($update){
				$ids = " AND emp_id = " . $emp_id . " AND loc_id = " . $loc_id;
				$this->buildlog('Updated Employee Contact Info', 'emploc', $ids, $trans_id);
				return $this->ok;
			
				} else {
					return $this->err;
				}	
			
			}
			
			
		} else {
			$this->err['desc'] = "Could not update employee info ";
			return $this->err;
		}
	
		
		
	}
	
	private function upempdept($depts, $emp_id, $trans_id){
		// add or remove employee from department
	
		if(!empty($depts) && is_numeric($emp_id)){
				
			// get current employee departments
			$this->db->where('emp_id', $emp_id);
			$query = $this->db->get('deptemp');
			
			$current_r = $query->result_array();
			$current = array();

			// clean up the array to just get the departments (not the id or emp_id) so we can compare what has been added. or removed
			for($a=0; $a < count($current_r); $a++){
				$current[] = $current_r[$a]['dept_id'];
			}
			
		    
		    // make sure all post the values are int
		   for($b=0; $b<count($depts); $b++){
		   		if(!is_numeric($depts[$b])){
		   				
		   			
		   			return;
					
					
		   		}
		   } 
			// This gets all the added departments that are not currently listed. 
			$added = array_values(array_diff($depts, $current));
			

			
			// add the added. 
			for($c=0; $c < count($added); $c++){
				$this->addempdept($added[$c], $emp_id, $trans_id);
			}
			
			$removed = array_values(array_diff($current, $depts));
			
			
			// remove the removed. 
			
			for($d=0; $d < count($removed); $d++){
				$this->remempdept($removed[$d], $emp_id, $trans_id);						
			}					
			
			// you can get the departments that have remained the same by doing: $same = array_intersect($new, $current);
			
				
			
		}
			
		
		
	}
	
	
	function addempdept($dept_id, $emp_id, $trans_id){
		
		if(is_numeric($dept_id) && is_numeric($emp_id)){
			$data = array('dept_id' => $dept_id, 'emp_id' => $emp_id);
			$this->db->insert('deptemp', $data);
			$newid = $this->db->insert_id();
		} else {
			$this->err['desc'] = "Could not add user to department ";
			return $this->err;
		}
		if($newid){
			$ids = " AND id = " . $newid;
			$this->buildlog('Employee Added to Department', 'deptemp', $ids, $trans_id);
			return $this->ok;
		} else {
			return $this->err;
		}		
		
	}
	
	function remempdept($dept_id, $emp_id, $trans_id){
	     
		 // remove employee from department
		if(is_numeric($dept_id) && is_numeric($emp_id)){
			$this->db->where('dept_id', $dept_id);
			$this->db->where('emp_id', $emp_id);
			$del = $this->db->delete('deptemp'); 
		} else {
			$this->err['desc'] = "Could not remove employee from department ";
			return $this->err;
		}
		if($del){
			$ids = " AND dept_id = " . $dept_id . " AND emp_id = " . $emp_id;
			$this->buildlog('Employee Removed from Department ', 'deptemp', $ids, $trans_id);
			return $this->ok;
		} else {
			return $this->err;
		}			 
	
	}
	

	
	function rememp($id) {
	 // deactivate employee
			
		if(is_numeric($id)) {
			$deactivated = date("Y-m-d H:i:s");
			$data = array('active' => 0, 'deactivated' => $deactivated);
			$this->db->where('id', $id);
			$update = $this->db->update('emp', $data);
		} else {
			$this->err['desc'] = "Could not remove employee";
			return $this->err;
		} 
		
		if($update){
			$this->db->where('id', $id);
			$query = $this->db->get('emp');
			$result = $query->result_array();
			
			$trans = 'Removed Employee: ' . $result[0]['fname'] . ' ' . $result[0]['lname'];
			$trans_id = $this->transaction($trans, 'rem-emp');
			$return = array( 'status' => 1, 'trans_id' => $trans_id );
			return $return;	
			
		} else {
			return $this->err;
		}
	}
	function actemp($id) {
		// reactivate employee
		if(is_numeric($id)) {
			$data['activedate'] = date("Y-m-d H:i:s");
			$data = array('active' => 1);
			$this->db->where('id', $id);
			$update = $this->db->update('emp', $data);
		} else {
			$this->err['desc'] = "Could not reactivate employee";
			return $this->err;
		} 
		
		if($update){
			$this->db->where('id', $id);
			$query = $this->db->get('emp');
			$result = $query->result_array();
			
			$trans = 'Activated Employee: ' . $result[0]['fname'] . ' ' . $result[0]['lname'];
			$trans_id = $this->transaction($trans, 'act-emp');
			$return = array( 'status' => 1, 'trans_id' => $trans_id );
			return $return;	
		} else {
			return $this->err;
		}
	
	}
	
	/*  ======================
	*	Log Features
	*	=======================
	*/
	
	private function transaction($desc, $type){
		$trans_time = date("Y-m-d H:i:s");
		
		$new_trans = array('user_id' => $this->user_id, 
						   'desc' => $desc, 
						   'trans_time' => $trans_time, 
						   'trans_type' => $type );
						   
		$this->db->insert('trans', $new_trans);
		$trans_id = $this->db->insert_id();
		return $trans_id;
	}
	
	private function buildlog($desc, $table, $query, $trans_id){
		// record log activity 
		$log = array();
		$log['trans_id'] = $trans_id;
		$log['logtime'] = date("Y-m-d H:i:s");
		$log['logentry'] = $desc;
		$log['log_table'] = $table;
		$log['log_q'] = $query;
		
		$this->db->insert('logs', $log);
	
	}
	
	function getnotify($whole = false){
			 
			if($whole){
				// get all the transaction information
			
				$this->db->order_by("trans_time", "DESC"); 
				$query = $this->db->get('trans', 100); // limit last 100 transactions
				$result=$query->result_array();
				$c = count($result);
				for($i=0;$i < $c; $i++){
						
					// get user information
					$this->db->where('id', $result[$i]['user_id']);
					$userq = $this->db->get('tmp_admin');
					$user = $userq->result_array();
					$result[$i]['admin_user'] = $user[0]['real_name'];
					
					// get all logs associated with transaction
					$this->db->where('trans_id', $result[$i]['id']);
					$logq = $this->db->get('logs');
					$logs = $logq->result_array();
					$clogs = count($logs);
					for($j=0; $j<$clogs; $j++){
						$dump = $this->db->query("SELECT * FROM " . $logs[$j]['log_table'] . " WHERE 1=1 " . $logs[$j]['log_q']);
						$logs[$j]['datadump'] = $dump->result_array();
					}
					$result[$i]['logs'] = $logs;
				}
			} else {
				$this->db->order_by("trans_time", "DESC"); 
				$query = $this->db->get('trans' , 10);
				$result=$query->result_array();
			}  
		
			
			$count = count($result);
			
			for($i=0; $i<$count;$i++){
				$result[$i]['since'] = $this->timeAgo($result[$i]['trans_time']);	
			}
			return $result;
	}
	
	private function timeAgo($timestamp){
    	$datetime1=new DateTime("now");
    	$datetime2=date_create($timestamp);
    	$diff=date_diff($datetime1, $datetime2);
    	$timemsg='';
	    if($diff->y > 0){
	       		 $timemsg = $diff->y .' year'. ($diff->y > 1?"s":'');
	
	    	} else if($diff->m > 0){
	     		$timemsg = $diff->m . ' month'. ($diff->m > 1?"s":'');
	    	} else if($diff->d > 0){
	     		$timemsg = $diff->d .' day'. ($diff->d > 1?"s":'');
	    	} else if($diff->h > 0){
	     		$timemsg = $diff->h .' hour'.($diff->h > 1 ? "s":'');
	   		} else if($diff->i > 0){
	     		$timemsg = $diff->i .' minute'. ($diff->i > 1?"s":'');
	    	} else if($diff->s > 0){
	     		$timemsg = $diff->s .' second'. ($diff->s > 1?"s":'');
	    }

			$timemsg = $timemsg.' ago';
			return $timemsg;
	}
	
	/*  ======================
	*	Check Functions
	*	=======================
	*/
	
	private function checkaddloc($emp_id = 0, $loc_id = 0, $contact = array(), $checkall = false, $trans_id){
				
			/* The If statement checks to see if the checkall value is true first. 
			 * If $checkall is set to true then the second parameter isn't needed, so the gate is open 
			 * If $checkall is set to false then the second function needs to be evaluated 
			 * So if checkall is false and anybody_home is set to true, then the gate is open
			 */
			
			if($checkall || $this->anybody_home($contact)){
				if($this->checkentry($emp_id, $loc_id)){
					$contact['emp_id'] = $emp_id;
					$contact['loc_id'] = $loc_id;
					$this->addemploc($contact, $trans_id);
				} else {
					$this->upemploc($emp_id, $loc_id, $contact, $trans_id);
				}
			}
			return;
		
	}
	
	private function checkentry($emp_id, $loc_id){
		// check if the employee already has location info stored. 
		$green = true;	
		$this->db->where('emp_id', $emp_id);
		$this->db->where('loc_id', $loc_id);
		$query = $this->db->get('emploc');
		$result = $query->result_array();
		if(!empty($result)){
			$green = false;
		}
		return $green;
	}

	private function checkadddeptloc($dept_id = 0, $loc_id = 0, $contact = array(), $trans_id){
			
			// add a location 
		  if($this->anybody_home_dept($contact)) {
			if($this->checkdeptentry($dept_id, $loc_id)){
				$contact['dept_id'] = $dept_id;
				$contact['loc_id'] = $loc_id;
				$this->adddeptloc($contact, $trans_id);
			} else {
				$this->updeptloc($dept_id, $loc_id, $contact, $trans_id);
			}
		  }
			
			return;
		
	}

	private function checkdeptentry($dept_id, $loc_id){
		// check if the department already has location info stored. 
		$green = true;	
		$this->db->where('dept_id', $dept_id);
		$this->db->where('loc_id', $loc_id);
		$query = $this->db->get('deptloc');
		$result = $query->result_array();
		if(!empty($result)){
			$green = false;
		}
		return $green;
	}
	
	private function anybody_home($arr){
			
		// check to see if any information has been passed for the location update. 
		$yup = false;
		foreach ($arr as $key => $value) {
			if(($key != 'sphone' && $key != 'slext' && $key != 'sfax') && $value != ''){
				$yup = true;
				break;
			}
		}
		
		return $yup;
	
	}
	
	private function anybody_home_dept($arr){
				// check to see if any information has been passed for the location update. 
		$yup = false;
		foreach ($arr as $key => $value) {
			if($value != ''){
				$yup = true;
				break;
			}
		}
		
		return $yup;
		
	}
	
	
}