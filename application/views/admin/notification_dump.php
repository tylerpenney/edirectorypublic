<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
            			<table class="table table-striped">
            				<?php
            				$t = $dump; 
            				$counti = count($t);
            				for($i=0; $i<$counti; $i++){
            				?> 
            				<tr>
            					<td><strong>Transaction: <?php echo $t[$i]['id'] ?> - <?php echo $t[$i]['desc'] ?></strong></td>
            					<td><?php echo $t[$i]['admin_user'] ?></td>
            					<td><?php echo date("D M, Y, g:i a", strtotime($t[$i]['trans_time'])); ?> - <?php echo $t[$i]['since']; ?></td>
            				</tr>
            				<tr>
            					<td colspan="3">
            						<?php 
            						  $countj = count($t[$i]['logs']);
									 for($j=0; $j<$countj; $j++){ 
									 ?>
            						<table class="table table-bordered">
            							<tr>
            								<td>Log: <?php echo $t[$i]['logs'][$j]['id']; ?>  - <?php echo $t[$i]['logs'][$j]['logentry']; ?> </td>
            								<td><?php echo $t[$i]['logs'][$j]['log_table']; ?> - <?php echo $t[$i]['logs'][$j]['log_q']; ?></td>
            								<td>
            									<div style="height:30px; overflow:auto;">
            									
            									<?php 
            										$countp = count($t[$i]['logs'][$j]['datadump']);
													for($p=0; $p < $countp; $p++){
														foreach($t[$i]['logs'][$j]['datadump'][$p] as $key => $value){
            												echo $key . ' : ' . $value . '<br />';
            											}
													}
            									 ?>
            									</div>
            								</td>
            							</tr>
            						</table>
            						<?php } ?> 
            					</td>
            				</tr>
            				<?php } ?>
            			</table>
            				<pre>
            				<?php // print_r($dump); ?>
            				</pre>
             </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row END OF PAGE -->