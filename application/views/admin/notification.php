<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
            				<?php 
            					$class = 'alert-info';
								$icon = 'fa fa-cog fa-spin'; 
								if(isset($type)){
									if($type == 'error'){
										$class = 'alert-danger';
										$icon = 'fa fa-exclamation-circle'; 
									} else if($type == 'success'){
										$class= 'alert-success';
										$icon = 'fa fa-check-square-o';
									} else if($type == 'info'){
										$class = 'alert-info';
										$icon = 'fa fa-cog fa-spin';
									}
								}
            				?>
            				<p class="alert <?php echo $class ?>" >
            				
        					<i class="<?php echo $icon; ?>"></i> 
        						<span style="margin-left:20px;">
        							 <?php if(isset($message) && $message != '') 
        							    echo $message;	?>
        							 <?php if(isset($link) && $link != '') 
        							    echo anchor($link, 'View Results');	?>
        						 </span> 
      						</p>  
             </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row END OF PAGE -->