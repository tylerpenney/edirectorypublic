<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	
            	<?php
            		$all = '';
					$aca = '';
					$naca = ''; 
            	if($this->uri->segment(3) == 'all'){
            		$all = 'class="dept_active"';	
            	} else if($this->uri->segment(3) == 'aca'){
            		$aca = 'class="dept_active"';
            	} else if($this->uri->segment(3) == 'naca'){
            		$naca = 'class="dept_active"';
            	}
            	?>
               <a href="<?php echo base_url() . 'index.php/diradmin/listdept/all' ?>" <?php echo $all ?> >All Departments</a> &nbsp;|&nbsp;
               <a href="<?php echo base_url() . 'index.php/diradmin/listdept/aca' ?>" <?php echo $aca ?> >Academic Departments</a> &nbsp;|&nbsp; 
               <a href="<?php echo base_url() . 'index.php/diradmin/listdept/naca' ?>" <?php echo $naca ?> >Non Academic Departments</a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTable">
                        <thead>
                            <tr>
                            	<th style="display:none">Alpha (Hidden)</th>
                                <th>Department Name</th>
                                <th>URL</th>
                                <th>Emp</th>
                                <th>SLO Build</th>
                                <th>SLO Contact</th>
                                <th>NC Build</th>
                                <th>NC Contact</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        	
                        	<?php
                        	$count = count($depts);
                        	 for($i=0; $i<$count; $i++) {?>
                            <tr>
                            	<td style="display:none;"><?php echo $depts[$i]['dname'][0]; ?></td>
                                <td><a href="<?php echo base_url() . 'index.php/diradmin/updept/' . $depts[$i]['id']  ?>" title="<?php echo $depts[$i]['ddesc']; ?>" ><?php echo $depts[$i]['dname']?></a></td>
                                <td>
              						<?php
          
                                	if($depts[$i]['durl'] == ''){
                                		  $url = '#';
                                	} else {
                                		$url = $depts[$i]['durl'];
                                	}
                                	?>
                                	
                                	<a href="<?php echo $url; ?>" title="<?php echo $depts[$i]['ddesc']; ?>">Link</a>
                                </td>
                                <td>
                                	<?php if($depts[$i]['numemp'] == ''){
                                		  $depts[$i]['numemp'] = '0';
                                	} ?>
                                	<?php echo anchor('diradmin/listemp/' . $depts[$i]['id'], $depts[$i]['numemp']) ?>
      								
                                </td>
                                <td><?php echo $depts[$i]['slo'][0]['lbuild']; ?></td>
                                <td><?php 
                                if($depts[$i]['slo'][0]['lphone'] != ''){
                                echo  $depts[$i]['slo'][0]['lphone'] . ' ';
								} 
								if($depts[$i]['slo'][0]['lext'] != '' ){
                                echo 'Ext: ' .  $depts[$i]['slo'][0]['lext'] . ' ';
								}
								if($depts[$i]['slo'][0]['lfax'] != ''){
                                echo 'Fax: ' .  $depts[$i]['slo'][0]['lfax'] . ' ';
								}
								 ?>
								</td>
                                <td><?php echo $depts[$i]['nc'][0]['lbuild']; ?></td>
                                <td><?php 
                                if($depts[$i]['nc'][0]['lphone'] != ''){
                                echo  $depts[$i]['nc'][0]['lphone'] . ' ';
								} 
								if($depts[$i]['nc'][0]['lext'] != ''){
                                echo 'Ext: ' .  $depts[$i]['nc'][0]['lext'] . ' ';
								}
								if($depts[$i]['nc'][0]['lfax'] != ''){
                                echo 'Fax: ' .  $depts[$i]['nc'][0]['lfax'] . ' ';
								}
								 ?>
								</td>
								<td>
									<a href="<?php echo base_url() . 'index.php/diradmin/updept/' . $depts[$i]['id']  ?>"><i class="fa fa-pencil-square"></i></a> &nbsp;&nbsp;
									<a href="<?php echo base_url() . 'index.php/diradmin/listemp/' . $depts[$i]['id']  ?>"><i class="fa fa-male"></i>
								</td>
                            </tr>
                           <?php } ?>
 
                       </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->