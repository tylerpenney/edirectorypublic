<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	Click the <i class="fa fa-bolt"></i> Icon to Re-activate Department
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTable">
                        <thead>
                            <tr>
                            	<th style="display:none">Alpha (Hidden)</th>
                                <th>Department Name</th>
                                <th>Department Description</th>
                                <th>URL</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        	
                        	<?php
                        	$count = count($depts);
                        	 for($i=0; $i<$count; $i++) {?>
                            <tr>
                            	<td style="display:none;"><?php echo $depts[$i]['dname'][0]; ?></td>
                            	<td><?php echo $depts[$i]['dname']?></td>
                            	<td><?php echo $depts[$i]['ddesc']?></td>
                                <td><?php echo $depts[$i]['durl']?> </td>
								<td><a href="<?php echo base_url() . 'index.php/dirajax/actdept/' . $depts[$i]['id']  ?>"<i class="fa fa-bolt"></i></td>
                            </tr>
                           <?php } ?>
 
                       </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->