<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Computer Services Department Employees
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>SLO</th>
                                <th>NC</th>
                                <th>SC</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tyler Penney</td>
                                <td>tyler_penney@cuesta.edu</td>
                                <td>Ext 2422</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                
                            </tr>
                            <tr>
                                <td>Steven Booth</td>
                                <td>sbooth@cuesta.edu</td>
                                <td>Ext 3423</td>
                                <td>N/A</td>
                                <td>N/A</td>
                               
                            </tr>
                            <tr>
                                <td>Donald Orr</td>
                                <td>donad_orr@cuesta.edu</td>
                                <td>Ext 2155</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                
                            </tr>
                            <tr>
                                <td>Lori McClain</td>
                                <td>lmclain@cuesta.edu</td>
                                <td>Ext 3524</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                
                            </tr>
                            <tr>
                                <td>Grant Chesy</td>
                                <td>gchesy@cuesta.edu</td>
                                <td>Ext 2522</td>
                                <td>Ext 2654</td>
                                <td>N/A</td>
                                
                            </tr>
                            <tr>
                                <td>Steve Budke</td>
                                <td>sbudke@cuesta.edu</td>
                                <td>Ext 2654</td>
                                <td>N/A</td>
                                <td>N/A</td>
                            </tr>
                            <tr>
                                <td>Eric McDonald</td>
                                <td>ecmdonald@cuesta.edu</td>
                                <td>Ext 2254</td>
                                <td>N/A</td>
                                <td>N/A</td>
                            </tr>
 
                                    </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->