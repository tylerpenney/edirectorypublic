<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Enter New Employee
            </div>
            <div class="panel-body">
                <div class="row">
                	<?php 
                	$attributes = array('id' => 'newemp');
				    echo form_open('dirajax/addemp', $attributes);
					?>

                    <div class="col-lg-6">
                       
                            <div class="form-group">
                                <label>Employee Name*</label>
                                <input class="form-control" name="fname" placeholder="First Name" required >
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="lname" placeholder="Last Name" required >
                            </div>
                            <div class="form-group">
                                <label>Email*</label>
                                <input class="form-control" name="email" placeholder="Enter Users Email">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="semail" value="1" checked >Display Email
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" name="emp_title" placeholder="Enter Employee Title" >
                            </div>
                            <div class="form-group">
                               <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="faculty" >Faculty
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                            	<label>Departments</label>
                            	 <select class="deptselector" name="dept[]" data-placeholder="Departments" multiple >
            	 		<?php foreach($depts as $dept){ 
					   			echo '<option value="' . $dept['id'] . '" >' . $dept['dname'] . '</option>' . PHP_EOL; 	
            	 		 		} ?>									
								 </select>
                            </div>
                          
						<input type="submit" class="btn btn-success" value="Create New Employee">
                        
                    </div>
                    <div class="col-lg-3">
                       
                            <div class="form-group">
                                <label>San Luis Obispo</label>
                                <input class="form-control" name="slo_lphone"  placeholder="Enter SLO Phone">
                            </div>
                             <div class="form-group">
                                <input class="form-control" name="slo_lext" placeholder="SLO Extenson">
                             </div>
                             <div class="form-group">
                                 <input class="form-control" name="slo_lfax" placeholder="SLO Fax">
                            </div>
                            
                       
                    </div>
                    <div class="col-lg-3">
                       
                              <div class="form-group">
                              	<label>North County</label>
                                <input class="form-control" name="nc_lphone" value="" placeholder="Enter NC Phone">
                              </div>
                              <div class="form-group">
                                <input class="form-control" name="nc_lext" placeholder="NC Extenson">
                              </div>
                               <div class="form-group">
                                <input class="form-control" name="nc_lfax" placeholder="NC Fax">
                               </div> 
                      
                    </div>
                    <div class="col-lg-3" >

                            <div class="form-group">
                            	<label>South County</label>
                                <input class="form-control" name="sc_lphone" value="" placeholder="Enter SC Phone">
                             </div>  
                            <div class="form-group">
                                <input class="form-control" name="sc_lext" placeholder="SC Extenson">
                             </div>  
                            <div class="form-group">
                                <input class="form-control" name="sc_lfax" placeholder="SC Fax">
                            </div>
                    </div>                                     
                
					</form>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row END OF PAGE -->