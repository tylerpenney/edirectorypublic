<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<?php 
            	if(isset($dept)){
            		echo anchor('diradmin/updept/' . $dept[0]['id'], $dept[0]['dname']); 
            	}
            	?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTable">
                        <thead>
                            <tr>
                            	<th style="display:none">Alpha (Hidden)</th>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Email</th>
                                <th>SLO Phone</th>
                                <th>SLO Ext</th>
                                <th>NC Phone </th>
                                <th>NC Ext</th>
                                <th>SC Info</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        	
                        	<?php
                        	$count = count($emp);
                        	 for($i=0; $i<$count; $i++) {?>
                            <tr>
                            	<td style="display:none;"><?php echo $emp[$i]['lname'][0]; ?></td>
                                <td><a href="<?php echo base_url() . 'index.php/diradmin/upemp/' . $emp[$i]['id']; ?>" ><?php echo $emp[$i]['fname'] . ' ' . $emp[$i]['lname']; ?></a></td>
                                <td><?php echo $emp[$i]['emp_title']; ?></td>
                                <td><?php echo $emp[$i]['email']; ?></td>
                                <td>
                                	<?php echo $emp[$i]['emploc']['slo'][0]['lphone']; ?>
                                </td>
                                <td>
								    <?php echo $emp[$i]['emploc']['slo'][0]['lext']; ?>
                                </td>
                                <td>
                            	    <?php echo $emp[$i]['emploc']['nc'][0]['lphone']; ?>
                                </td>
                                <td>
									<?php echo $emp[$i]['emploc']['nc'][0]['lext']; ?>
                                </td>
                                <td></td>
								<td><a href="<?php echo base_url() . 'index.php/diradmin/upemp/' . $emp[$i]['id']  ?>"<i class="fa fa-pencil-square"></i></td>
                            </tr>
                           <?php } ?>
 
                       </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->