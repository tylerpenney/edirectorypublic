<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
				Click the <i class="fa fa-bolt"></i> Icon to Re-activate Employee
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTable">
                        <thead>
                            <tr>
                            	<th style="display:none">Alpha (Hidden)</th>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Email</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        	
                        	<?php
                        	$count = count($emp);
                        	 for($i=0; $i<$count; $i++) {?>
                            <tr>
                            	<td style="display:none;"><?php echo $emp[$i]['lname'][0]; ?></td>
                                <td><?php echo $emp[$i]['fname'] . ' ' . $emp[$i]['lname']; ?></td>
                                <td><?php echo $emp[$i]['email']; ?></td>
                                <td><?php echo $emp[$i]['emp_title']; ?></td>
								<td><a href="<?php echo base_url() . 'index.php/dirajax/actemp/' . $emp[$i]['id']  ?>"<i class="fa fa-bolt"></i></td>
                            </tr>
                           <?php } ?>
 
                       </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->