<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               Edit Employee Information
            </div>
            <div class="panel-body">
                <div class="row">
                	<?php 
                	$attributes = array('id' => 'upemp');
				    echo form_open('dirajax/upemp', $attributes);
					?>
                    <div class="col-lg-6">

                            <div class="form-group">
                                <label>Employee Name</label>
                                <input class="form-control" name="fname" placeholder="First Name" value="<?php echo $emp[0]['fname']; ?>" >
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="lname" placeholder="Last Name" value="<?php echo $emp[0]['lname']; ?>" >
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="email" placeholder="Enter Users Email" value="<?php echo $emp[0]['email']; ?>" >
                                <div class="checkbox">
                                    <label>
                                    	<?php 
                                    	$semail = '';
										if($emp[0]['semail'] == 1){
										$semail = ' checked ';
										}
                                    	?>
                                        <input type="checkbox" name="semail" value="1" <?php echo $semail; ?> >Display Email
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" name="emp_title" placeholder="Enter Employee Title" value="<?php echo $emp[0]['emp_title']; ?>" >
                            </div>
                            <div class="form-group">
                               <div class="checkbox">
                                	<?php 
                                	  $faculty = '';
									  if($emp[0]['faculty'] == 1){
									   		$faculty = ' checked ';
									  }
                                	?>
                                    <label>
                                        <input type="checkbox" value="1" name="faculty" <?php echo $faculty ?> >Faculty
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                            	<label>Departments</label>
                            	 <select class="deptselector" name="dept[]" data-placeholder="Departments" multiple >
            	 		<?php foreach($depts as $dept){ 
            	 	   				$mem = '';
					   				if(is_numeric($dept['mem'])){
					   					$mem = ' selected ';
					   				}
					   					echo '<option value="' . $dept['id'] . '" ' . $mem .' >' . $dept['dname'] . '</option>' . PHP_EOL; 	
            	 		 		} ?>
								 </select>
                            </div>
                          
						<input type="submit" class="btn btn-success" value="Update Employee">
                        
                    </div>
                    <div class="col-lg-3">
                       
                            <div class="form-group">
                            	
                                <label>San Luis Obispo</label>
                            	<?php if($slo[0]['lphone'] == ''){
                            		$slo_lphone = '546-3100';
                            	} else {
                            		$slo_lphone = $slo[0]['lphone'];
                            	} ?>
                                <input class="form-control" name="slo_lphone"  placholder="Enter SLO Phone" value="<?php echo $slo_lphone; ?>" />
                                <div class="checkbox">
                                	<?php 
                                	  $sphone = '';
									  if($slo[0]['sphone'] == 1){
									   		$sphone = ' checked ';
									  }
                                	?>
                                    <label>
                                        <input type="checkbox" value="1" name="slo_sphone" <?php echo $sphone ?> >Display SLO Phone
                                    </label>
                                </div>
                                 <input class="form-control" name="slo_lext" placeholder="SLO Extenson" value="<?php echo $slo[0]['lext']; ?>" />
                                <div class="checkbox">
                                 	<?php 
                                	  $slext = '';
									  if($slo[0]['slext'] == 1){
									   		$slext = ' checked ';
									  }
                                	?>                               	
                                    <label>
                                        <input type="checkbox" value="1"  name="slo_slext" <?php echo $slext ?> >Display SLO Extension
                                    </label>
                                </div>
                                 <input class="form-control" name="slo_lfax" placeholder="SLO Fax" value="<?php echo $slo[0]['lfax']; ?>" />
                                <div class="checkbox">
                                 	<?php 
                                	  $sfax = '';
									  if($slo[0]['sfax'] == 1 ){
									   		$sfax = ' checked ';
									  }
                                	?>                                 
                                    <label>
                                        <input type="checkbox" value="1" name="slo_sfax" <?php echo $sfax ?>  >Display SLO Fax
                                    </label>
                                </div>
                            </div>
                            
                       
                    </div>
                    <div class="col-lg-3">

                       
                            <div class="form-group">
                                <label>North County</label>
                                <input class="form-control" name="nc_lphone" placeholder="Enter NC Phone" value="<?php echo $nc[0]['lphone']; ?>" >
                                <div class="checkbox">
                                	<?php 
                                	  $sphone = '';
									  if($nc[0]['sphone'] == 1){
									   		$sphone = ' checked ';
									  }
                                	?>
                                    <label>
                                        <input type="checkbox" value="1" name="nc_sphone" <?php echo $sphone ?>  >Display NC Phone
                                    </label>
                                </div>
                                 <input class="form-control" name="nc_lext" placeholder="NC Extenson" value="<?php echo $nc[0]['lext']; ?>" >
                                <div class="checkbox">
                                	<?php 
                                	  $slext = '';
									  if($nc[0]['slext'] == 1){
									   		$slext = ' checked ';
									  }
                                	?>
                                    <label>
                                        <input type="checkbox" value="1"  name="nc_slext" <?php echo $slext ?> >Display NC Extension
                                    </label>
                                </div>
                                 <input class="form-control" name="nc_lfax" placeholder="NC Fax" value="<?php echo $nc[0]['lfax']; ?>" >
                                <div class="checkbox">
                                	<?php 
                                	  $sfax = '';
									  if($nc[0]['sfax'] == 1){
									   		$sfax = ' checked ';
									  }
                                	?>
                                    <label>
                                        <input type="checkbox" value="1" name="nc_sfax" <?php echo $sfax; ?> > Display NC Fax
                                    </label>
                                </div>
                            </div>
                            
                      
                    </div>
                    <div class="col-lg-3" >

                    	<button type="button" id="showSC" class="btn btn-outline btn-primary btn-xs">Enter South County</button>
                       		<?php 
                       			$hide = 'display:none; ';
                       		if(!empty($sc)){
                       			$hide = '';
                       		} ?>
                            <div class="form-group" id="southcounty" style="<?php echo $hide; ?> margin-top:10px;">
                                <label>South County</label>
                                <input class="form-control" name="sc_lphone" placeholder="Enter SC Phone" value="<?php echo $sc[0]['lphone']; ?>">
                                <div class="checkbox">
                                <?php 
                                	  $sphone = '';
									  if($sc[0]['sphone'] == 1){
									   		$sphone = ' checked ';
									  }
                                	?>	
                                    <label>
                                        <input type="checkbox" value="1" name="sc_sphone" <?php echo $sphone ?>  >Display SC Phone
                                    </label>
                                </div>
                                 <input class="form-control" name="sc_lext" placeholder="SC Extenson" value="<?php echo $sc[0]['lext']; ?>" >
                                <div class="checkbox">
                                	<?php 
                                	  $slext = '';
									  if($sc[0]['slext'] == 1){
									   		$slext = ' checked ';
									  }
                                	?>                                	
                                    <label>
                                        <input type="checkbox" value="1"  name="sc_slext" <?php echo $slext ?>  >Display SC Extension
                                    </label>
                                </div>
                                 <input class="form-control" name="sc_lfax" placeholder="SC Fax"  value="<?php echo $sc[0]['lfax']; ?>" >
                                <div class="checkbox">
                                	<?php 
                                	  $sfax = '';
									  if($sc[0]['sfax'] == 1){
									   		$sfax = ' checked ';
									  }
                                	?>                                	
                                    <label>
                                        <input type="checkbox" value="1" name="sc_sfax" <?php echo $sfax; ?>  >Display SC Fax
                                    </label>
                                </div>
                            </div>                                        
                    </div>
                    <input type="hidden" name="emp_id" value="<?php echo $emp_id ?>"/>
					</form>
							<div class="col-lg-3" style="border-top:1px solid #CCCCCC;" >

								<?php  echo form_open('dirajax/rememp');  ?>
								<input type="hidden" name="emp_id" value="<?php echo  $emp_id; ?>" />
								<br />
                				<div class="form-group" style="margin-top:10px;" >

                    			<input type="submit" class="btn btn-warning" value="Remove Employee" />
                				</div>
               			 		</form> 
              	                     
        					</div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row END OF PAGE -->