<ul class="dropdown-menu dropdown-alerts">
	<?php foreach($notifications as $n){ 
	switch($n['trans_type']) {
		case 'up-dept' :
		case 'new-dept' :
			$icn = 'fa-users';
		break;
		case 'up-emp' :
		case 'new-emp' :
			$icn ='fa-user';
		break;
		case 'rem-dept':
		case 'rem-emp':
			$icn = 'fa-times-circle';
		break;
		case 'act-emp':
		case 'act-dept':
			$icn = 'fa-eye';
		break;
		default:
			$icn = 'fa-user';
		
	}	
		?>
	<li>
        <a href="<?php echo $n['id']; ?>">
            <div>
                <i class="fa <?php echo $icn;?> fa-fw"></i> <?php echo $n['desc']; ?>
                <span class="pull-right text-muted small"><?php echo $n['since']; ?></span>
            </div>
        </a>
    </li>
     <li class="divider"></li>
	<?php } ?>

  
    <li>
        <a class="text-center" href="<?php echo base_url() ?>index.php/diradmin/notifications/">
            <strong>See All Notifications</strong>
            <i class="fa fa-angle-right"></i>
        </a>
    </li>
</ul>
<!-- /.dropdown-alerts -->