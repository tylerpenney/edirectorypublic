<div class="panel-body">
    <div class="row">
    			<?php
                	$attributes = array('id' => 'updept');
				    echo form_open('dirajax/updept', $attributes);
				?>
				<input type="hidden" name="dept_id" value="<?php echo $dept_id; ?>" />
        <div class="col-lg-6">
           
                <div class="form-group">
                    <label>Department Name*</label>
                    <input class="form-control" name="dname"  value="<?php echo $dept[0]['dname']; ?>">
                </div>
                <div class="form-group">
                    <label>URL*</label>
                    <input class="form-control" name="durl" value="<?php echo $dept[0]['durl']; ?>">
                </div>
                <div class="form-group">
                    <label>Department Description*</label>
                    <textarea class="form-control" name="ddesc" rows="3"><?php echo $dept[0]['ddesc']; ?> </textarea>
                </div>
                <div class="checkbox">
                	<?php 
                        $daca = '';
						if($dept[0]['daca'] == 1){
							$daca = ' checked ';
						}
                        ?>
                    <label>
                        <input type="checkbox" value="1" name="daca"  <?php echo $daca; ?> >Academic Department
                    </label>
                </div>
                <div class="checkbox">
                 	<?php 
                        $dnon = '';
						if($dept[0]['dnon'] == 1){
							$dnon = ' checked ';
						}
                        ?>	
               
                     <label>
                         <input type="checkbox" value="1" name="dnon" <?php echo $dnon; ?> >Academic Support Department
                     </label>
                 </div>
				<input type="submit" class="btn btn-success" value="Update Department" />
            
        </div>
        <div class="col-lg-3">
           
                <div class="form-group">
                    <label>SLO Building or Room</label>
                    <input class="form-control" name="slo_lbuild"  value="<?php echo $slo[0]['lbuild'] ?>">
                </div>
                <div class="form-group">
                	<label>SLO Dept Phone</label>	
                    <input class="form-control" name="slo_lphone" value="<?php echo $slo[0]['lphone'] ?>" >
                </div>
                <div class="form-group">
                	<label>SLO Dept Extension</label>
                    <input class="form-control" name="slo_lext" value="<?php echo $slo[0]['lext'] ?>" >
                </div>
                <div class="form-group">
                	<label>SLO Fax</label>
                    <input class="form-control" name="slo_lfax" value="<?php echo $slo[0]['lfax'] ?>" >
                </div>

        </div>
        <div class="col-lg-3">
           
           
                <div class="form-group">
                    <label>NC Building or Room</label>
                    <input class="form-control" name="nc_lbuild"  value="<?php echo $nc[0]['lbuild'] ?>">
                </div>
                <div class="form-group">
                	<label>NC Dept Phone</label>	
                    <input class="form-control" name="nc_lphone" value="<?php echo $nc[0]['lphone'] ?>" >
                </div>
                <div class="form-group">
                	<label>NC Dept Extension</label>
                    <input class="form-control" name="nc_lext" value="<?php echo $nc[0]['lext'] ?>" >
                </div>
                <div class="form-group">
                	<label>NC Fax</label>
                    <input class="form-control" name="nc_lfax" value="<?php echo $nc[0]['lfax'] ?>" >
                </div>
                
        </div>
        <div class="col-lg-3" style="border-top:1px solid #CCCCCC;" >

                <div class="form-group" style="margin-top:10px;" >
                    <label>SC Building or Room</label>
                    <input class="form-control" name="sc_lbuild"  value="<?php echo $sc[0]['lbuild'] ?>" >
                </div>
                <div class="form-group">
                	<label>SC Dept Phone</label>	
                    <input class="form-control" name="sc_lphone" value="<?php echo $sc[0]['lphone'] ?>" >
                </div>
                <div class="form-group">
                	<label>SC Dept Extension</label>
                    <input class="form-control" name="sc_lext" value="<?php echo $sc[0]['lext'] ?>" >
                </div>
                <div class="form-group">
                	<label>SC Fax</label>
                    <input class="form-control" name="sc_lfax" value="<?php echo $sc[0]['lfax'] ?>" >
                </div>
                                                  
        </div>
		</form>
		<div class="col-lg-3" style="border-top:1px solid #CCCCCC;" >
			    <?php 
                		$disabled = "";
						$count = "0";
						if($dept[0]['numemp'] > 0){
							$disabled = "disabled";
							$count = $dept[0]['numemp'];
						} 
                	?>
				<?php  echo form_open('dirajax/remdept');  ?>
				<input type="hidden" name="dept_id" value="<?php echo $dept_id; ?>" />
				<input type="hidden" name="numemp" value="<?php echo $count; ?>" />
				<br />
                <div class="form-group" style="margin-top:10px;" >

                    <input type="submit" <?php echo $disabled;?> class="btn btn-warning" value="Remove Department" />
                </div>
                </form> 
                <p>There are currently <?php echo $count;?>  Employees in this Department.</p>                            
        </div>
    </div>
    <!-- /.row (nested) -->
</div>
<!-- /.panel-body -->