 <div class="col-lg-12">
 			<div class="well">
                <h4>Welcome to the new Cuesta eDirectory</h4>
                <p>DataTables is a very flexible, advanced tables plugin for jQuery. In SB Admin, we are using a specialized version of DataTables built for Bootstrap 3. We have also customized the table headings to use Font Awesome icons in place of images. For complete documentation on DataTables, visit their website at <a target="_blank" href="https://datatables.net/">https://datatables.net/</a>.</p>
                <a class="btn btn-success btn-lg btn-block" target="_blank" href="https://datatables.net/">Add New Employee</a>
                <a class="btn btn-primary btn-lg btn-block" target="_blank" href="https://datatables.net/">Add New Department</a>
                <a class="btn btn-warning btn-lg btn-block" target="_blank" href="https://datatables.net/">View Employees</a>
                <a class="btn btn-info btn-lg btn-block" target="_blank" href="https://datatables.net/">View Departments</a>
            </div>
 </div>