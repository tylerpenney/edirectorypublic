<div class="panel-body">
    <div class="row">
    			<?php
                	$attributes = array('id' => 'newdept');
				    echo form_open('dirajax/adddept', $attributes);
				?>
        <div class="col-lg-6">
           
                <div class="form-group">
                    <label>Department Name*</label>
                    <input class="form-control" name="dname" placeholder="Enter Department Name">
                </div>
                <div class="form-group">
                    <label>URL*</label>
                    <input class="form-control" name="durl" value="http://">
                </div>
                <div class="form-group">
                    <label>Department Description*</label>
                    <textarea class="form-control" name="ddesc" rows="3"> </textarea>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="1" name="daca"  >Academic Department
                    </label>
                </div>
                <div class="checkbox">
                     <label>
                         <input type="checkbox" value="1" name="dnon" checked >Academic Support Department
                     </label>
                 </div>
				<input type="submit" class="btn btn-success" value="Create New Department" />
            
        </div>
        <div class="col-lg-3">
           
                <div class="form-group">
                    <label>SLO Information</label>
                    <input class="form-control" name="slo_lbuild" placeholder="Enter SLO Building or Room ">
                </div>
                <div class="form-group">
                    <input class="form-control" name="slo_lphone" placeholder="Enter SLO Dept Phone" >
                </div>
                <div class="form-group">
                    <input class="form-control" name="slo_lext" placeholder="Enter SLO Dept Extension" >
                </div>
                <div class="form-group">
                    <input class="form-control" name="slo_lfax" placeholder="Enter SLO Fax" >
                </div>

        </div>
        <div class="col-lg-3">
           
           
                <div class="form-group">
                    <label>NC Information</label>
                    <input class="form-control" name="nc_lbuild" placeholder="Enter NC Building or Room ">
                </div>
                <div class="form-group">
                    <input class="form-control" name="nc_lphone" placeholder="Enter NC Dept Phone" >
                </div>
                <div class="form-group">
                    <input class="form-control" name="nc_lext" placeholder="Enter NC Dept Extension" >
                </div>
                <div class="form-group">
                    <input class="form-control" name="nc_lfax" placeholder="Enter NC Fax" >
                </div>
                
        </div>
        <div class="col-lg-3" >
        	<button type="button" id="showSC" class="btn btn-outline btn-primary btn-xs">Enter South County</button>
           
                <div id="southcounty" style="display:none; margin-top:10px;">
					<div class="form-group">
                        <label>SC Information</label>
                        <input class="form-control" name="sc_lbuild" placeholder="Enter SC Building or Room ">
                    </div>
                    <div class="form-group">
                   
                        <input class="form-control" name="sc_lphone" placeholder="Enter SC Dept Phone" >
                    </div>
                    <div class="form-group">
                    
                        <input class="form-control" name="sc_lext" placeholder="Enter SC Dept Extension" >
                    </div>
                    <div class="form-group">
                      
                        <input class="form-control" name="sc_lfax" placeholder="Enter SC Fax" >
                    </div>
                
                </div>                                        
        </div>
		</form>
    </div>
    <!-- /.row (nested) -->
</div>
<!-- /.panel-body -->