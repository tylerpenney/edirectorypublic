<!DOCTYPE html>
<html>
	<head>
	 <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">	
		
    <!-- jQuery Version 1.11.0 -->
    <script src="<?php echo base_url(); ?>js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
			    <!-- Custom Fonts -->
    		<link href="<?php echo base_url(); ?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	</head>
<body>
<div class="container">
<div class="row">
	<h1><?php echo $emp[0]['fname'] . ' ' . $emp[0]['lname']; ?></h1>
	<h4><?php echo $emp[0]['depts']; ?></h4>
	<h3><a href="mailto:<?php echo $emp[0]['email']; ?>"><?php echo $emp[0]['email'] ?></a></h3>
	

	<table class="table table-striped">
	  <tbody>
	  		<?php if(!empty($emp[0]['slo'])){ ?>
        	<tr>
              <td><?php echo $emp[0]['slo']; ?></td>
      		</tr>
      		<?php } ?>
	  		<?php if(!empty($emp[0]['nc'])){ ?>
        	<tr>
              <td><?php echo $emp[0]['nc']; ?></td>
      		</tr>
      		<?php } ?>
      		<?php if(!empty($emp[0]['sc'])){ ?>
        	<tr>
              <td><?php echo $emp[0]['sc']; ?></td>
      		</tr>
      		<?php } ?>
      </tbody>
	</table>

</div>
</div>