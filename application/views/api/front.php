<!DOCTYPE html>
<html>
	<head>
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>js/plugins/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/plugins/jqueryui/autocomplete.css" media="screen" />
						    <!-- Custom Fonts -->
    	<link href="<?php echo base_url(); ?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	 <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">	
		
    <!-- jQuery Version 1.11.0 -->
    <script src="<?php echo base_url(); ?>js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    
        <!-- jQuery UI-->
    <script src="<?php echo base_url(); ?>js/plugins/jqueryui/jquery-ui.min.js"></script>
    
   <script type="text/javascript" src="<?php echo base_url();?>js/plugins/fancybox/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/plugins/jqueryui/autocomplete-pub.js"></script>
			

		<style>
			#main {margin-top:20px;}
			.fancybox {display:block}
			.alpha {padding:6px 4px !important;}
			#alphatop {text-align:center; font-size:larger;}
			.panel-info:before{
				display: block; 
				content: " "; 
				margin-top: -285px; 
				height: 285px; 
				visibility: hidden; 
			}
			.totop {float:right; color:white;}
			#footer{ border-top:1px solid #cccccc; text-align:center}
			#footertext{padding:20px;}
			
		</style>
		<script>
			<!-- highlights box clicked on -->
		$( document ).ready(function() {
			
			$('.fancybox').fancybox();
			
			$(".alpha").click(function() {
				var davis = $(this).attr("href");
	    		$(davis).css( {"background-color": "#31708f", "color": "white"});
	    		$(davis).append("<a class='totop' href='#alphatop'> Back to Top </a>");
	    		$(davis + "-box").css({"border-color": "#31708f"});
	    		
		
			setTimeout(function() { 
				$(davis).css( {"background-color": "#d9edf7", "color" : "#31708f" });
				$(davis + "-box").css({"border-color": "#bce8f1"});
				$(".totop").css({"color": "#31708f"});
			}, 5000);
			});
			
				
			
			
		});
		
		$(document).on("click", '.totop', function (e) {
			$(this).remove();
		});
		
		

	
		</script>
	</head>
<body>
<div class="container" id="main">
	<div class="row">
		<div class="col-xs-12 col-sm-4">
			<div class="well" >
		     	<div class="input-group custom-search-form">
                 	<input type="text" class="form-control" id="auto" placeholder="Search eDirectory...">
                      	 <span class="input-group-btn">
                            	<button class="btn btn-default" type="button">
                                    	<i class="fa fa-search"></i>
                            	</button>
                         </span>
             	</div>	
            </div>
		</div>
		<div class="col-xs-12 col-sm-8">
			<div class="well" id="alphatop">
				<div class="btn-group btn-group-justified" role="group" >
		<?php foreach( range('a','z') as $letter){ ?>
				
			   <a href="#alpha-<?php echo $letter; ?>" class="btn btn-default alpha" role="button" ><?php echo strtoupper($letter); ?></a>
		<?php } ?>
				</div>
			</div>
		</div>
	</div>
    <div class="row">
      <div class="col-xs-12 col-sm-4  ">
      		<?php foreach($left as $akey=>$depts){ ?>
        	<div class="panel panel-info" id="alpha-<?php echo $akey; ?>-box">
        		 <div class="panel-heading" id="alpha-<?php echo $akey; ?>"><?php echo strtoupper($akey); ?></div>
  				 <div class="panel-body">
  				 	<?php foreach($depts as $dept){ ?>
                   <a href="<?php echo base_url().'index.php/dirapi/dept/'.$dept['id']?>" class="fancybox fancybox.iframe" data-int="<?= $dept['id']?>" id="dept-<?= $dept['id']?>"><?= $dept['dname']?></a>
                    <?php } ?>
  				 </div>
        	</div><!-- alpha box -->
        	<?php } ?>
      </div><!-- main column -->
      <div class="col-xs-12 col-sm-4  ">
      	      		<?php foreach($center as $akey=>$depts){ ?>
        	<div class="panel panel-info" id="alpha-<?php echo $akey; ?>-box" >
        		 <div class="panel-heading" id="alpha-<?php echo $akey; ?>" ><?php echo strtoupper($akey); ?></div>
  				 <div class="panel-body">
  				 	<?php foreach($depts as $dept){ ?>
                   <a href="<?php echo base_url().'index.php/dirapi/dept/'.$dept['id']?>" class="fancybox fancybox.iframe" id="dept-<?php echo $dept['id']?>"><?php echo $dept['dname']?></a>
                    <?php } ?>
  				 </div>
        	</div><!-- alpha box -->
        	<?php } ?>
      </div>
      <div class="col-xs-12 col-sm-4  ">
      	     <?php foreach($right as $akey=>$depts){ ?>
        	<div class="panel panel-info" id="alpha-<?php echo $akey; ?>-box">
        		 <div class="panel-heading" id="alpha-<?php echo $akey; ?>"><?php echo strtoupper($akey); ?></div>
  				 <div class="panel-body">
  				 	<?php foreach($depts as $dept){ ?>
                   <a href="<?php echo base_url().'index.php/dirapi/dept/'.$dept['id']?>" class="fancybox fancybox.iframe" id="dept-<?php echo $dept['id']?>"><?php echo $dept['dname']?></a>
                    <?php } ?>
  				 </div>
        	</div><!-- alpha box -->
        	<?php } ?>
      </div>

  </div>
</div>
<div id="footer" class="container-fluid">
    <div class="row">
    	<div class="col-xs-12 " id="footertext">
    	<?php echo anchor('login', '&copy;'); ?> 2015 Cuesta College
    	</div
    </div>
</div>
</body>
</html>