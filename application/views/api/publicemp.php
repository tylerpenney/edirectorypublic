<!DOCTYPE html>
<html>
	<head>
		
			<link href="<?php echo base_url(); ?>css/plugins/dataTables.bootstrap.css" rel="stylesheet">
			
	 		<!-- Bootstrap Core CSS -->
    		<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">	
		
    		<!-- jQuery Version 1.11.0 -->
    		<script src="<?php echo base_url(); ?>js/jquery-1.11.0.js"></script>

    		<!-- Bootstrap Core JavaScript -->
    		<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
			    <!-- DataTables JavaScript -->
    		<script src="<?php echo base_url(); ?>js/plugins/dataTables/jquery.dataTables.min.js"></script>
   				 <!-- <script src="<?php echo base_url(); ?>js/plugins/dataTables/jquery.dataTables.alpha.js"></script> -->
    		<script src="<?php echo base_url(); ?>js/plugins/dataTables/dataTables.bootstrap.js"></script>
    		<script>
    			$(document).ready(function(){
				    var table = $('#dataTable').DataTable( {
				        dom: 'Alfrtip'
				    } );
				});
    		</script>
    		<style>
    			p {margin:0px; padding:0px;}
    		</style>

	</head>
<body>
<div class="container">
	<div class="row">
		<h3><?php echo $dept[0]['dname']; ?></h3>
		<?php $count = count($emp); ?>
		<h5>Number of Employees: <?php echo $count ?></h5>
		<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTable">
                        <thead>
                            <tr>
                            	<th style="display:none">Alpha (Hidden)</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                        	
                        	<?php
                        	$count = count($emp);
                        	 for($i=0; $i<$count; $i++) {?>
                            <tr>
                            	<td style="display:none;"><?php echo $emp[$i]['lname'][0]; ?></td>
                                <td><?php echo $emp[$i]['fname'] . ' ' . $emp[$i]['lname']; ?></td>
                                <td><?php echo $emp[$i]['email']; ?></td>
    							<td>
    								<?php 
										if($emp[$i]['slo'] != '' ){
										echo '<p>' . $emp[$i]['slo'] . '</p>';
										}
										if($emp[$i]['nc'] != '' ){
										echo '<p>' .  $emp[$i]['nc'] . '</p>';
										}
										if($emp[$i]['sc'] != '' ){
										echo '<p>' .  $emp[$i]['sc'] . '</p>';
										}
										
    								?>
    							</td>
                            </tr>
                           <?php } ?>
 
                       </tbody>
                    </table>
		</div> <!-- /.table-responsive -->
	</div> <!-- /.row -->
</div> <!-- /.container -->
</body>
</html>
