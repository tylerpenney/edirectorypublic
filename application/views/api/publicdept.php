<!DOCTYPE html>
<html>
	<head>
	 <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">	
		
    <!-- jQuery Version 1.11.0 -->
    <script src="<?php echo base_url(); ?>js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
			    <!-- Custom Fonts -->
    		<link href="<?php echo base_url(); ?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	</head>
<body>
<div class="container">
<div class="row">
	<h1><?php echo $dept[0]['dname'] ?></h1>
	<h4><?php echo $dept[0]['ddesc'] ?></h4>
	<?php if($dept[0]['durl'] != '') { ?>
	<p><a href="<?php echo $dept[0]['durl'] ?>" target="_parent"><i class="fa fa-globe"></i> <?php echo $dept[0]['durl'] ?></a></p>
	<?php } ?>
	<p><?php echo anchor('dirapi/empdept/' . $dept[0]['id'], '<i class="fa fa-users"></i> View ' . $dept[0]['dname'] . ' Employees') ?>  </p>
	

	<table class="table table-striped">
	  <tbody>
	  		<?php if(!empty($slo[0]['slo'])){ ?>
        	<tr>
              <td><?php echo $slo[0]['slo']; ?></td>
      		</tr>
      		<?php } ?>
	  		<?php if(!empty($nc[0]['nc'])){ ?>
        	<tr>
              <td><?php echo $nc[0]['nc']; ?></td>
      		</tr>
      		<?php } ?>
      		<?php if(!empty($sc[0]['sc'])){ ?>
        	<tr>
              <td><?php echo $sc[0]['sc']; ?></td>
      		</tr>
      		<?php } ?>
      </tbody>
	</table>

</div>
</div>