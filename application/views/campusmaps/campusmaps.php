<!DOCTYPE html >
<html >
<head>
	<title>Cuesta Campus Map</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
		<script src="<?php echo base_url()?>js/plugins/maphilight/jquery.imagemapster.js"></script>
		<script>
		$( document ).ready(function() {

    		$('#sloMap').mapster({
        		fillOpacity: 0.4,
        		fillColor: "d42e16",
        		stroke: true,
        		strokeColor: "d42e16",
       			strokeOpacity: 0.8,
        		strokeWidth: 3,
        		singleSelect: true,
        		mapKey: 'name',
        		listKey: 'name',
        		showToolTip: true,
        		toolTipClose: ["tooltip-click", "area-click"],
        		areas: [
            		{
                	key: "b<?php echo $location; ?>",
                	selected: true
            		}
            	]
    		});
    		
    		$('#ncMap').mapster({
        		fillOpacity: 0.4,
        		fillColor: "d42e16",
        		stroke: true,
        		strokeColor: "d42e16",
       			strokeOpacity: 0.8,
        		strokeWidth: 3,
        		singleSelect: true,
        		mapKey: 'name',
        		listKey: 'name',
        		showToolTip: true,
        		toolTipClose: ["tooltip-click", "area-click"],
        		areas: [
            		{
                	key: "<?php echo $location; ?>",
                	selected: true
            		}
            	]
    		});
    		
			});
		</script>
</head>
<body bgcolor="#ffffff">
	<?php if($campus == 'slo'){ ?>
<div style="width:893px; margin-left:auto; margin-right:auto;">
<img name="campusmap" src="<?php echo base_url()?>img/campusmap.png" width="893" height="783" id="sloMap" usemap="#m_campusmap" alt=""  />
<map name="m_campusmap" id="m_campusmap">
<area shape="poly" coords="170,281,189,274,195,290,177,298,170,281" href="b8100" title="8100" alt="8100" name="b8100" />
<area shape="poly" coords="211,243,226,228,237,227,237,232,256,231,257,240,233,239,227,249,226,259,223,259,224,270,210,270,211,243" href="b8000" title="8000" alt="8000" name="b8000" />
<area shape="rect" coords="768,677,808,718" href="b7400-7500" title="7400-7500" alt="7400-7500" name="b7400-7500" />
<area shape="rect" coords="688,668,730,732" href="b1900" title="1900" alt="1900" name="b1900" />
<area shape="poly" coords="266,469,265,464,271,458,276,460,281,455,286,459,300,473,294,479,295,481,289,487,286,485,280,491,262,474,266,469" href="b2400" title="2400" alt="2400" name="b2400" />
<area shape="poly" coords="505,362,505,380,493,381,493,390,463,390,462,386,465,385,465,378,478,363,505,362" href="b5400" title="5400" alt="5400" name="b5400" />
<area shape="poly" coords="468,402,481,403,489,410,489,411,503,411,504,430,498,431,497,428,478,429,477,431,467,430,468,402" href="b5300" title="5300" alt="5300" name="b5300" />
<area shape="poly" coords="505,410,505,430,516,430,516,426,532,426,531,418,540,419,540,395,512,395,512,402,505,410" href="b5200" title="5200" alt="5200" name="b5200" />
<area shape="poly" coords="506,359,505,380,513,381,513,393,541,393,541,376,547,377,548,352,517,351,517,358,506,359" href="b5100" title="5100" alt="5100" name="b5100" />
<area shape="rect" coords="603,372,618,396" href="b1700" title="1700" alt="1700" name="b1700" />
<area shape="poly" coords="495,468,483,468,483,441,510,441,510,452,496,453,496,468,495,468" href="b1300" title="1300" alt="1300" name="b1300" />
<area shape="rect" coords="507,463,558,488" href="b1100" title="1100" alt="1100" name="b1100" />
<area shape="rect" coords="555,493,586,550" href="b1000" title="1000" alt="1000" name="b1000" />
<area shape="rect" coords="440,507,457,532" href="b1200" title="1200" alt="1200" name="b1200" />
<area shape="poly" coords="482,510,533,510,533,534,482,534,482,510" href="b1600" title="1600" alt="1600" name="b1600" />
<area shape="poly" coords="480,567,479,606,496,606,496,615,535,615,535,577,549,577,549,563,535,563,535,559,497,559,497,567,480,567" href="b1400" title="1400" alt="1400" name="b1400" />
<area shape="poly" coords="265,535,293,507,309,524,282,552,265,535" href="b2200" title="2200" alt="2200" name="b2200" />
<area shape="poly" coords="231,567,261,538,279,556,249,585,231,567" href="b2500" title="2500" alt="2500" name="b2500" />
<area shape="rect" coords="54,571,81,611" href="b4700" title="4700" alt="4700" name="b4700" />
<area shape="poly" coords="116,608,153,571,144,562,150,556,163,569,160,572,162,573,165,572,169,577,171,577,178,584,171,591,164,584,127,624,119,616,120,614,116,608" href="4000" title="4000" alt="4000" name="4000" />
<area shape="poly" coords="95,549,124,521,136,531,107,561,95,549" href="b4500" title="4500" alt="4500" name="b4500" />
<area shape="poly" coords="75,495,91,496,85,538,69,536,75,495" href="b4600" title="4600" alt="4600" name="b4600" />
<area shape="poly" coords="219,501,247,530,236,543,228,535,230,532,217,518,214,521,206,514,219,501" href="b2300" title="2300" alt="2300" name="b2300" />
<area shape="poly" coords="183,530,193,519,202,528,191,539,183,530" href="b2700" title="2700" alt="2700" name="b2700" />
<area shape="poly" coords="170,520,182,507,190,515,178,528,170,520" href="b2600" title="2600" alt="2600" name="b2600" />
<area shape="poly" coords="162,505,173,493,181,502,171,513,162,505" href="b2900" title="2900" alt="2900" name="b2900" />
<area shape="poly" coords="185,484,205,503,197,510,178,491,185,484" href="b2800" title="2800" alt="2800" name="b2800" />
<area shape="poly" coords="193,475,213,495,207,502,187,482,193,475" href="b2000" title="2000" alt="2000" name="b2000" />
<area shape="poly" coords="200,469,232,437,246,451,243,455,250,461,221,489,200,469" href="b2100" title="2100" alt="2100" name="b2100" />
<area shape="poly" coords="131,447,146,432,168,454,154,468,131,447" href="b4400" title="4400" alt="4400" name="b4400" />
<area shape="poly" coords="91,467,123,436,126,437,135,429,133,427,134,425,124,416,83,458,91,467" href="b4200" title="4200" alt="4200" name="b4200" />
<area shape="poly" coords="177,445,222,402,204,385,160,428,177,445" href="b4100" title="4100" alt="4100" name="b4100" />
<area shape="poly" coords="142,405,170,378,158,365,151,372,153,374,139,389,137,386,129,393,142,405" href="b4300" title="4300" alt="4300" name="b4300" />
<area shape="poly" coords="423,235,409,250,442,283,460,265,467,272,481,259,483,260,450,295,470,316,468,318,479,330,486,324,488,324,498,313,491,305,482,314,478,310,475,312,472,309,498,283,483,268,487,264,495,271,504,263,487,245,501,233,488,220,486,222,481,218,435,265,432,262,442,252,423,235" href="b7100" title="7100" alt="7100" name="b7100" />
<area shape="poly" coords="390,195,417,168,415,167,430,151,444,164,443,165,447,170,446,172,480,206,441,244,390,195" href="b7300" title="7300" alt="7300" name="b7300" />
<area shape="poly" coords="334,174,347,160,393,205,379,219,334,174" href="b6100" title="6100" alt="6100" name="b6100" />
<area shape="poly" coords="290,226,302,216,331,244,319,256,290,226" href="b6200" title="6200" alt="6200" name="b6200" />
<area shape="poly" coords="282,214,293,204,286,196,274,206,282,214" href="b6800" title="6800" alt="6800" name="b6800" />
<area shape="poly" coords="272,205,282,195,275,186,264,196,272,205" href="b6700" title="6700" alt="6700" name="b6700" />
<area shape="poly" coords="268,190,277,180,270,171,259,182,268,190" href="b6600" title="6600" alt="6600" name="b6600" />
<area shape="poly" coords="348,325,377,295,383,301,388,294,423,329,421,333,424,337,423,339,432,348,400,379,392,371,390,373,386,368,383,371,346,335,352,329,348,325" href="b3100-3200" title="3100-3200" alt="3100-3200" name="b3100-3200" />
<area shape="poly" coords="282,380,322,341,337,356,344,363,343,364,350,372,311,410,282,380" href="b3300-3400" title="3300-3400" alt="3300-3400" name="b3300-3400" />
<area shape="poly" coords="323,215,343,195,365,214,362,218,368,224,372,220,378,228,353,253,347,245,349,242,343,235,339,238,321,220,325,217,323,215" href="b6300" title="6300" alt="6300" name="b6300" />
</map>
</div>
<?php } ?>
<?php if($campus == 'nc'){ ?>
<div style="width:750px; margin-left:auto; margin-right:auto;">
<img name="campusmapNC" src="<?php echo base_url()?>img/campusmapNC.png" width="749" height="567" id="ncMap" usemap="#m_campusmapNC" alt="" />
<map name="m_campusmapNC" id="m_campusmapNC">
<area shape="poly" coords="640,413,684,364,702,381,659,430,640,413" href="N9800" title="N9800" alt="N9800" name="N9800" />
<area shape="poly" coords="613,272,665,208,744,271,692,336,613,272" href="N3100-N3200" title="N3100-N3200" alt="N3100-N3200" name="N3100-N3200" />
<area shape="poly" coords="538,330,557,308,572,321,555,343,538,330" href="N2800" title="N2800" alt="N2800" name="N2800" />
<area shape="poly" coords="490,206,515,227,493,256,518,275,542,247,570,269,526,321,511,309,509,311,512,315,507,322,448,275,453,269,458,272,460,270,445,257,490,206" href="N2400" title="N2400" alt="N2400" name="N2400" />
<area shape="rect" coords="367,149,429,178" href="N4000" title="N4000" alt="N4000" name="N4000" />
<area shape="rect" coords="211,236,265,265" href="N5000" title="N5000" alt="N5000" name="N5000" />
<area shape="rect" coords="211,192,265,222" href="N3000" title="N3000" alt="N3000" name="N3000" />
<area shape="rect" coords="211,140,265,169" href="N6000" title="N6000" alt="N6000" name="N6000" />
<area shape="poly" coords="278,236,305,235,304,253,352,253,352,281,304,280,305,261,278,261,278,236" href="N2000" title="N2000" alt="N2000" name="N2000" />
<area shape="poly" coords="278,140,304,140,304,162,330,162,330,141,354,141,354,172,329,172,329,192,305,192,305,172,278,171,278,140" href="N1000" title="N1000" alt="N1000" name="N1000" />
</map>
</div>
<?php } ?>
</body>
</html>
