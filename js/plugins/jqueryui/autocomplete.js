		/*
		 * jQuery UI Autocomplete: Load Data via AJAX
		 * http://salman-w.blogspot.com/2013/12/jquery-ui-autocomplete-examples.html
		 */

		$(function() {
			var adminurl = 'http://localhost/directory/index.php/diradmin/upemp/';
			var adminurldept = 'http://localhost/directory/index.php/diradmin/updept/';
			$("#autocomplete").autocomplete({
				delay: 500,
				minLength: 3,
				source: function(request, response) {
					$.getJSON("http://localhost/directory/index.php/dirapi/usersearch/?callback=?", {
						q: request.term,
						page_limit: 10
					}, function(data) {
						// data is an array of objects and must be transformed for autocomplete to use
						var array = data.error ? [] : $.map(data.emp, function(em) {
							return {
								label: em.name,
								url: adminurl + em.id,
								slo: em.slo,
								nc: em.nc,
								dept: em.depts
							};
						});
						response(array);
					});
				},
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
				},
				select: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// navigate to the selected item's url
					window.open(ui.item.url);
				}
			}).data("ui-autocomplete")._renderItem = function(ul, item) {
				var $a = $("<a></a>");
				$("<span class='m-name'></span>").text(item.label).appendTo($a);
				$("<span class='m-dept'></span>").text(item.dept).appendTo($a);
				
				// if slo phone, if nc phone, if sc phone
				if(item.slo){
					$("<span class='m-phone'></span>").text(item.slo).appendTo($a);	
				}
				if(item.nc){
					$("<span class='m-phone'></span>").text(item.nc).appendTo($a);	
				}
				
				return $("<li></li>").append($a).appendTo(ul);
			};
			
			$("#deptauto").autocomplete({
				delay: 500,
				minLength: 3,
				source: function(request, response) {
					$.getJSON("http://localhost/directory/index.php/dirapi/deptsearch/?callback=?", {
						q: request.term,
						page_limit: 10
					}, function(data) {
						// data is an array of objects and must be transformed for autocomplete to use
						var array = data.error ? [] : $.map(data.dept, function(d) {
							return {
								label: d.dname,
								url: adminurldept + d.id,
								slo: d.slo,
								nc: d.nc,
								sc: d.sc,
							};
						});
						response(array);
					});
				},
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
				},
				select: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// navigate to the selected item's url
					window.open(ui.item.url);
				}
			}).data("ui-autocomplete")._renderItem = function(ul, item) {
				var $a = $("<a></a>");
				$("<span class='m-name'></span>").text(item.label).appendTo($a);
				// if slo phone, if nc phone, if sc phone
				if(item.slo){
					$("<span class='m-phone'></span>").text(item.slo).appendTo($a);	
				}
				if(item.nc){
					$("<span class='m-phone'></span>").text(item.nc).appendTo($a);	
				}
				if(item.sc){
					$("<span class='m-phone'></span>").text(item.sc).appendTo($a);	
				}
				
				return $("<li></li>").append($a).appendTo(ul);
			};
		});