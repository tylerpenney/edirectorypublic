		/*
		 * jQuery UI Autocomplete: Load Data via AJAX
		 * http://salman-w.blogspot.com/2013/12/jquery-ui-autocomplete-examples.html
		 */

		$(function() {
			
			var empurl = 'http://localhost/directory/index.php/dirapi/emp/';
			var depturl = 'http://localhost/directory/index.php/dirapi/dept/';
			
			
			$("#auto").autocomplete({
				delay: 500,
				minLength: 3,
				source: function(request, response) {
					$.getJSON("http://localhost/directory/index.php/dirapi/allsearch/?callback=?", {
						q: request.term,
						page_limit: 10
					}, function(data) {
						// data is an array of objects and must be transformed for autocomplete to use
						var array = data.error ? [] : $.map(data.empdept, function(em) {
							return {
								emp: em.name,
								dname: em.dname,
								type: em.type, 
								id: em.id,
								slo: em.slo,
								nc: em.nc,
								sc: em.sc,
								dept: em.depts
							};
						});
						response(array);
					});
				},
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
				},
				select: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					
					// clear search
					this.value = "";
    				
					
					// set the corect URL (dept vs emp) 
					
					if(ui.item.type == 'emp'){
						var uri = empurl + ui.item.id; 
					} else {
						var uri = depturl + ui.item.id; 
					}
					
					// make sure fancybox is loaded in the scripts! 
					if(typeof $.fancybox == 'function') {
    					$.fancybox.open({
   							 href: uri,
    						 type: 'iframe'
						});
					} else { 
						// Load lamely
    					window.open(uri);
    				}
    				return false;

				}
			}).data("ui-autocomplete")._renderItem = function(ul, item) {
				var $a = $("<a></a>");
				if(item.type == 'emp'){
					$("<span class='m-name'></span>").text(item.emp).appendTo($a);
					$("<span class='m-dept'></span>").text(item.dept).appendTo($a);
				
				// if slo phone, if nc phone, if sc phone
					if(item.slo){
						$("<span class='m-phone'></span>").text(item.slo).appendTo($a);	
					}
					if(item.nc){
						$("<span class='m-phone'></span>").text(item.nc).appendTo($a);	
					}
				
				} else { // this is a department 
					$("<span class='m-name'></span>").text(item.dname).appendTo($a);
				// if slo phone, if nc phone, if sc phone
					if(item.slo){
						$("<span class='m-phone'></span>").text(item.slo).appendTo($a);	
					}
					if(item.nc){
						$("<span class='m-phone'></span>").text(item.nc).appendTo($a);	
					}
					if(item.sc){
					$("<span class='m-phone'></span>").text(item.sc).appendTo($a);	
					}
				}
				
				return $("<li></li>").append($a).appendTo(ul);
			};
			
		});
