$(document).ready(function(){
	
	var path =  'http://localhost/directory/';

 	 $( "#showSC" ).click(function() {
  		$( "#southcounty" ).toggle();
  		 event.preventDefault();
	 });
	 
    var table = $('#dataTable').DataTable( {
        dom: 'Alfrtip'
    } );
    
    $('[title!=""]').qtip(); 
	 
	$(".deptselector").chosen({
   		 disable_search_threshold: 10,
   		 no_results_text: "Oops, nothing found!",
   		 width: "100%"
 	 });
 	 
 	 // check boxes when entering values for SLO
 	 $( "input[name='slo_lphone']" ).change(function() {
 	 	  if( !$(this).val() ) {
          		$("input[name='slo_sphone']").prop("checked", false);
    	  } else {
        		$("input[name='slo_sphone']").prop("checked", true);
    	  }
	 });
	  	 
 	 $( "input[name='slo_lext']" ).change(function() {
 	 	  if( !$(this).val() ) {
          		$("input[name='slo_slext']").prop("checked", false);
    	  } else {
        		$("input[name='slo_slext']").prop("checked", true);
    	  }
	 });
	  	
 	 $( "input[name='slo_lfax']" ).change(function() {
 	 	  if( !$(this).val() ) {
          		$("input[name='slo_sfax']").prop("checked", false);
    	  } else {
        		$("input[name='slo_sfax']").prop("checked", true);
    	  }
	 });
	 
	  	 // check boxes when entering values for NC
 	 $( "input[name='nc_lphone']" ).change(function() {
 	 	  if( !$(this).val() ) {
          		$("input[name='nc_sphone']").prop("checked", false);
    	  } else {
        		$("input[name='nc_sphone']").prop("checked", true);
    	  }
	 });
	  	 
 	 $( "input[name='nc_lext']" ).change(function() {
 	 	 if( !$(this).val() ) {
          		$("input[name='nc_slext']").prop("checked", false);
    	  } else {
        		$("input[name='nc_slext']").prop("checked", true);
    	  }
	 });
	  	
 	 $( "input[name='nc_lfax']" ).change(function() {
 	 	  if( !$(this).val() ) {
          		$("input[name='nc_sfax']").prop("checked", false);
    	  } else {
        		$("input[name='nc_sfax']").prop("checked", true);
    	  }
	 });
	 
	 	  	 // check boxes when entering values for NC
 	 $( "input[name='sc_lphone']" ).change(function() {
 	 	  if( !$(this).val() ) {
          		$("input[name='sc_sphone']").prop("checked", false);
    	  } else {
        		$("input[name='sc_sphone']").prop("checked", true);
    	  }
	 });
	  	 
 	 $( "input[name='sc_lext']" ).change(function() {
 	 	  if( !$(this).val() ) {
          		$("input[name='sc_slext']").prop("checked", false);
    	  } else {
        		$("input[name='sc_slext']").prop("checked", true);
    	  }
	 });
	  	
 	 $( "input[name='sc_lfax']" ).change(function() {
 	 	  if( !$(this).val() ) {
          		$("input[name='sc_sfax']").prop("checked", false);
    	  } else {
        		$("input[name='sc_sfax']").prop("checked", true);
    	  }
	 });
	 
	 
	 // override jquery validate plugin defaults to make it work with bootstrap
	$.validator.setDefaults({
	    highlight: function(element) {
	        $(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    }
	});
		 
 	$('#newemp').validate({
		rules: {
			fname: {
				required: true
			},
			lname: {
				required: true
			},
			email: {
				required: true,
				email: true
			}
		},
		messages: {
			fname: "Please enter first name",
			lname: "Please enter last name",
			email: "Please enter a valid @cuesta.edu email"
		}
	});
	

	
	$('#upemp').validate({
		rules: {
			fname: {
				required: true
			},
			lname: {
				required: true
			},
			email: {
				required: true,
				email: true
			}
		},
		messages: {
			fname: "Please enter first name",
			lname: "Please enter last name",
			email: "Please enter a valid @cuesta.edu email"
		}
	});
	
	$('#newdept').validate({
		rules: {
			dname: {
				required: true
			},
			durl: {
				required: true,
				url: true
			},
			ddesc: {
				required: true
			}
		},
		messages: {
			dname: "Please enter Department Name",
			durl: "Please enter link ",
			ddesc: "Please enter Department Description"
		}
	});
	
	$('#editdept').validate({
		rules: {
			dname: {
				required: true
			},
			durl: {
				required: true,
				url: true
			},
			ddesc: {
				required: true
			}
		},
		messages: {
			dname: "Please enter Department Name",
			durl: "Please enter link ",
			ddesc: "Please enter Department Description"
		}
	});
		
	
}); 